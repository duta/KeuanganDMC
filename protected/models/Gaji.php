<?php

/**
 * This is the model class for table "gaji".
 *
 * The followings are the available columns in table 'gaji':
 * @property integer $g_id
 * @property integer $g_pegawai
 * @property integer $g_tanggal
 * @property integer $g_gaji
 * @property integer $g_gaji_perjam
 * @property string $g_tanggal_insert
 */
class Gaji extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gaji';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('g_pegawai, g_tanggal, g_gaji, g_gaji_perjam, g_tanggal_insert', 'required'),
			array('g_pegawai, g_gaji, g_gaji_perjam', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('g_id, g_pegawai, g_tanggal, g_gaji, g_gaji_perjam, g_tanggal_insert', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pegawai' => array(self::BELONGS_TO, 'Pegawai', 'g_pegawai'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'g_id' => 'G',
			'g_pegawai' => 'Pegawai',
			'g_tanggal' => 'Tanggal',
			'g_gaji' => 'Gaji',
			'g_gaji_perjam' => 'Gaji Perjam',
			'g_tanggal_insert' => 'Tanggal Insert',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->addCondition("g_pegawai=".$id);
		$criteria->compare('g_id',$this->g_id);
		$criteria->compare('g_pegawai',$this->g_pegawai);
		$criteria->compare('g_tanggal',$this->g_tanggal);
		$criteria->compare('g_gaji',$this->g_gaji);
		$criteria->compare('g_gaji_perjam',$this->g_gaji_perjam);
		$criteria->compare('g_tanggal_insert',$this->g_tanggal_insert,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Gaji the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDataGaji($id)
	{
		$return = Yii::app()->db->createCommand("SELECT * FROM gaji  WHERE g_pegawai = $id ORDER BY g_tanggal DESC LIMIT 1")->queryRow();
        return $return;
	}
}
