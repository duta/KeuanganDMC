<?php

/**
 * This is the model class for table "realisasi_sppd".
 *
 * The followings are the available columns in table 'realisasi_sppd':
 * @property integer $rs_id
 * @property integer $rs_header
 * @property integer $rs_biaya
 * @property string $rs_jenis
 * @property string $rs_file
 * @property integer $rs_status
 */
class RealisasiSppd extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'realisasi_sppd';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rs_header', 'required'),
			array('rs_header, rs_biaya, rs_status', 'numerical', 'integerOnly'=>true),
			array('rs_jenis', 'length', 'max'=>50),
			array('rs_file', 'length', 'max'=>80),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('rs_id, rs_header, rs_biaya, rs_jenis, rs_file, rs_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'rs_id' => 'Rs',
			'rs_header' => 'Rs Header',
			'rs_biaya' => 'Rs Biaya',
			'rs_jenis' => 'Rs Jenis',
			'rs_file' => 'Rs File',
			'rs_status' => 'Rs Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('rs_id',$this->rs_id);
		$criteria->compare('rs_header',$this->rs_header);
		$criteria->compare('rs_biaya',$this->rs_biaya);
		$criteria->compare('rs_jenis',$this->rs_jenis,true);
		$criteria->compare('rs_file',$this->rs_file,true);
		$criteria->compare('rs_status',$this->rs_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RealisasiSppd the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function insertRealisasi($id_rsh,$rs_biaya,$rs_jenis,$rs_file)
	{
		$sql = "INSERT INTO `realisasi_sppd` (`rs_header`,`rs_biaya`,`rs_jenis`,`rs_file`)
				VALUES (".$id_rsh.",".$rs_biaya.",'".$rs_jenis."','".$rs_file."')";
		Yii::app()->db->createCommand($sql)->query();
	}

	public function detailRealisasi($id)
	{
		$sql = "SELECT rs_biaya,rs_jenis,rs_file
				FROM realisasi_sppd
				WHERE rs_header = ".$id;
		$return = Yii::app()->db->createCommand($sql)->queryAll();
        return $return;
	}

	public function deleteAllRecord($rs_header)
	{
		$sql = "DELETE FROM `realisasi_sppd` WHERE `rs_header` = ".$rs_header;
		Yii::app()->db->createCommand($sql)->query();
	}
	
}
