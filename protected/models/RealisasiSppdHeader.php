<?php

/**
 * This is the model class for table "realisasi_sppd_header".
 *
 * The followings are the available columns in table 'realisasi_sppd_header':
 * @property integer $rsh_id
 * @property integer $rsh_sppd
 * @property string $rsh_tanggal
 * @property integer $rsh_total
 * @property integer $rsh_status
 */
class RealisasiSppdHeader extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'realisasi_sppd_header';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rsh_sppd, rsh_tanggal, rsh_total, rsh_status', 'required'),
			array('rsh_sppd, rsh_total, rsh_status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('rsh_id, rsh_sppd, rsh_tanggal, rsh_total, rsh_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'rsh_id' => 'Rsh',
			'rsh_sppd' => 'Rsh Sppd',
			'rsh_tanggal' => 'Rsh Tanggal',
			'rsh_total' => 'Rsh Total',
			'rsh_status' => 'Rsh Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('rsh_id',$this->rsh_id);
		$criteria->compare('rsh_sppd',$this->rsh_sppd);
		$criteria->compare('rsh_tanggal',$this->rsh_tanggal,true);
		$criteria->compare('rsh_total',$this->rsh_total);
		$criteria->compare('rsh_status',$this->rsh_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RealisasiSppdHeader the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDataTahun(){

		$sql = new CSqlDataProvider("SELECT YEAR(rsh_tanggal) tahun
										FROM realisasi_sppd_header GROUP BY YEAR(rsh_tanggal) ORDER BY YEAR(rsh_tanggal) ASC",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}
	
	public function updateStatus($id,$total)
	{
		$sql = "UPDATE realisasi_sppd_header SET rsh_status = 1, rsh_total = ".$total." WHERE `rsh_id` = ".$id;
		Yii::app()->db->createCommand($sql)->query();
	}

	public function getDataById($id)
	{
		$return = Yii::app()->db->createCommand("SELECT 
					realisasi_sppd_header.*,
					s_alasan,
					s_tanggal,
					proyek.p_nama as proyek,
					proyek.p_id as proyek_id,
					p_nama_lengkap
				FROM realisasi_sppd_header
				INNER JOIN sppd ON s_id = rsh_sppd
				LEFT JOIN proyek ON proyek.p_id = s_proyek 
				LEFT JOIN pegawai ON pegawai.p_id = s_pegawai WHERE rsh_id = ".$id)->queryRow();
        return $return;
	}

	function getDataRealisasiSPPD()
	{
		$sql = "SELECT 
					realisasi_sppd_header.*,
					s_alasan,
					s_tanggal,
					proyek.p_nama as proyek,
					proyek.p_id as proyek_id,
					p_nama_lengkap
				FROM realisasi_sppd_header
				INNER JOIN sppd ON s_id = rsh_sppd
				LEFT JOIN proyek ON proyek.p_id = s_proyek 
				LEFT JOIN pegawai ON pegawai.p_id = s_pegawai
				WHERE 
				 s_pegawai = ".Yii::app()->user->getState('idUser');
		
		$sql = new CSqlDataProvider($sql,
				array(
                    'pagination' => false,
                ));

        return $sql;
	}

	function getDataReportRealisasiSPPD()
	{
		$sql = "SELECT 
					realisasi_sppd_header.*,
					s_alasan,
					s_tanggal,
					proyek.p_nama as proyek,
					proyek.p_id as proyek_id,
					p_nama_lengkap
				FROM realisasi_sppd_header
				INNER JOIN sppd ON s_id = rsh_sppd
				LEFT JOIN proyek ON proyek.p_id = s_proyek 
				LEFT JOIN pegawai ON pegawai.p_id = s_pegawai
				WHERE rsh_status = 1 
				";
		if(Yii::app()->user->getState('_level') == 'pegawai'){
			$sql .= "AND s_pegawai = ".Yii::app()->user->getState('idUser');
		};
		$sql = new CSqlDataProvider($sql,
				array(
                    'pagination' => false,
                ));

        return $sql;
	}

	function getDataKonfirmasiRealisasiSPPD()
	{
		$sql = "SELECT 
					realisasi_sppd_header.*,
					s_alasan,
					s_tanggal,
					proyek.p_nama as proyek,
					proyek.p_id as proyek_id,
					p_nama_lengkap
				FROM realisasi_sppd_header
				INNER JOIN sppd ON s_id = rsh_sppd
				LEFT JOIN proyek ON proyek.p_id = s_proyek 
				LEFT JOIN pegawai ON pegawai.p_id = s_pegawai
				WHERE rsh_status = 1";
		
		$sql = new CSqlDataProvider($sql,
				array(
                    'pagination' => false,
                ));

        return $sql;
	}

	public function updateTotalSPPD($rsh_id,$rsh_total)
    {
    	// $sq = "SELECT `rsh_total` AS sppd FROM `realisasi_sppd_header` WHERE `rsh_id` = ".$rsh_id;
    	// $LastSPPD = Yii::app()->db->createCommand($sq)->queryRow();
    	// $sppd = $LastSPPD['sppd']+$rsh_total;
    	$sppd = $rsh_total;
    	$sql = "UPDATE `realisasi_sppd_header` SET `rsh_total` = ".$sppd." WHERE `rsh_id` = ".$rsh_id;
		Yii::app()->db->createCommand($sql)->query();
    }
}
