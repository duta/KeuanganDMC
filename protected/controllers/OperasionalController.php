<?php

class OperasionalController extends Controller
{

	public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('Sppd','Kas','SppdSave','Lembur','LemburSave','SaveDataLembur','SaveDataSppd','LemburIndex', 'SppdIndex'),
                'expression'=>"Yii::app()->controller->isValidationUser()",
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
                'deniedCallback' => array($this, 'redirecting'),
            ),
        );
    }

    function isValidationUser() {
        if(Yii::app()->user->getState('_level')=='admin' || Yii::app()->user->getState('_level')=='koordinator')
            return true;
        return false;
	}

    public function redirecting(){
        $this->redirect(array('/Login'));
    }

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionKas()
	{
		$model=new Transaksi;
		$model1=new Transaksi;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Transaksi']))
		{
			$model->attributes=$_POST['Transaksi'];
			$model1->attributes=$_POST['Transaksi'];

			//Admin
			$model->t_pegawai = Yii::app()->user->getState('idUser');
			$model->t_tanggal = date('Y-m-d',strtotime($_POST['Transaksi']['t_tanggal']));
			$model->t_keterangan = $_POST['Transaksi']['t_keterangan'];
			$model->t_coa = 0;
			$model->t_debit = 0;
			$model->t_kredit = $_POST['Transaksi']['t_nominal'];
			$model->t_tanggal_insert = date("Y-m-d H:i:s");
			$model->t_tanggal_update = date("Y-m-d H:i:s");
			$model->save();

			//Pegawai
			$model1->t_pegawai = $_POST['Transaksi']['t_pegawai'];
			$model1->t_tanggal = date('Y-m-d',strtotime($_POST['Transaksi']['t_tanggal']));
			$model1->t_keterangan = $_POST['Transaksi']['t_keterangan'];
			$model1->t_debit = $_POST['Transaksi']['t_nominal'];
			$model1->t_coa = 0;
			$model1->t_kredit = 0;
			$model1->t_tanggal_insert = date("Y-m-d H:i:s");
			$model1->t_tanggal_update = date("Y-m-d H:i:s");
			$model1->save();

			$this->redirect(array('Kas'));
		}

		$this->render('kas',array(
			'model'=>$model,
		));
	}

	public function actionSppd()
	{
		$modelPegawai = new Pegawai();
        $modelSppd = new Sppd();

		if(isset($_POST['Filter'])){

			$bulan = $_POST['Filter']['bulan'];
            $tahun = $_POST['Filter']['tahun'];
            $pegawai = $_POST['Filter']['pegawai'];
            $status = $_POST['Filter']['status'];

			$dataSppd = $modelSppd->getSppdAdminFilter($pegawai,$status,$bulan,$tahun)->getData();
			$this->renderPartial('tableSppd',array(
				'dataSppd'=>$dataSppd,
				'status'=>$status,
			));
		}else{

			$dataTahun = $modelSppd->getDataTahun()->getData();
	        $dataPegawai = $modelPegawai->getDataPegawaiForFilter()->getData();

			$this->render('sppd', array(
				'dataTahun' => $dataTahun,
	            'dataPegawai'=>$dataPegawai
			));
		}

	}

	public function actionSppdIndex()
	{
        $modelSppd = new Sppd();
		$dataSppd = $modelSppd->getSppdAdminFilter('000', 0, '000', '000')->getData();
		$this->renderPartial('tableSppd',array(
			'dataSppd'=>$dataSppd,
			'status'=>0,
		));
	}

	public function actionSaveDataSppd()
	{
		$model = new Sppd();
		$count = count($_POST['s_id']);
		for ($i=0; $i < $count ; $i++) {
			$id = $_POST['s_id'][$i];
			$model->updateStatus($id,1);
		}
	}

	public function actionSppdSave()
	{
		$modelSppd = new Sppd();

		$s_id = $_POST['sppd'];
		$selected = $_POST['selected'];

		if($selected == 'done'){
			$status = 1;
		}else{
			$status = 0;
		}
		$modelSppd->updateStatus($s_id,$status);
	}

	public function actionLembur()
	{
		
		$modelPegawai = new Pegawai();
        $modelLembur = new Lembur();

		if(isset($_POST['Filter'])){

			$bulan = $_POST['Filter']['bulan'];
            $tahun = $_POST['Filter']['tahun'];
            $pegawai = $_POST['Filter']['pegawai'];
            $status = $_POST['Filter']['status'];

			$dataLembur = $modelLembur->getLemburAdminFilter($pegawai,$status,$bulan,$tahun)->getData();
			$this->renderPartial('tableLembur',array(
				'dataLembur'=>$dataLembur,
				'status'=>$status,
			));
		}else{

			$dataTahun = $modelLembur->getDataTahun()->getData();
        	$dataPegawai = $modelPegawai->getDataPegawaiForFilter()->getData();

			$this->render('lembur', array(
				'dataTahun' => $dataTahun,
	            'dataPegawai'=>$dataPegawai
			));
		}
	}

	public function actionLemburIndex()
	{
        $modelLembur = new Lembur();
		$dataLembur = $modelLembur->getLemburAdminFilter('000',0,'000','000')->getData();
		$this->renderPartial('tableLembur',array(
			'dataLembur'=>$dataLembur,
			'status'=>0,
		));
	}

	public function actionSaveDataLembur()
	{
		$modelLembur = new Lembur();
		$count = count($_POST['l_id']);
		for ($i=0; $i < $count ; $i++) {
			$id = $_POST['l_id'][$i];
			$modelLembur->updateStatus($id,1);
		}
	}

	public function actionLemburSave()
	{
		$modelLembur = new Lembur();

		$s_id = $_POST['lembur'];
		$selected = $_POST['selected'];

		if($selected == 'done'){
			$status = 1;
		}else{
			$status = 0;
		}
		$modelLembur->updateStatus($s_id,$status);
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}