<?php

class KeuanganController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
    {
        return array(
        	array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('Pengeluaran','DeleteKeuangan','GetDataKeuangan','SaveCashFlow'),
                'expression'=>"Yii::app()->user->getState('_level')=='admin'",
            ),
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('Pengeluaran','DeleteKeuangan','GetDataKeuangan'),
                'expression'=>"Yii::app()->user->getState('_level')=='pegawai'",
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
                'deniedCallback' => array($this, 'redirecting'),
            ),
        );
    }

    public function redirecting(){
        $this->redirect(array('/Login'));
    }

    public function actionDeleteKeuangan(){
    	$model=new Transaksi;
    	$id = $_POST['SendData']['id'];
    	$model->updateDihapus($id);

    }

	public function actionGetDataKeuangan(){
    	$model=new Transaksi;
    	$id = $_POST['SendData']['id'];
    	echo json_encode($model->detailTransaksi($id));

    }

    public function actionSaveCashFlow()
    {
    	$modelUpdateTransaksi=$this->loadModel($_POST['Transaksi']['t_id_cash_flow']);
        $modelUpdateTransaksi->attributes=$_POST['Transaksi'];
        $modelUpdateTransaksi->t_pegawai = $_POST['Transaksi']['t_pegawai'];
        $modelUpdateTransaksi->t_tanggal = date('Y-m-d',strtotime($_POST['Transaksi']['t_tanggal_cash_flow']));
        $modelUpdateTransaksi->t_debit = $_POST['Transaksi']['t_nominal_cash_flow'];
        $modelUpdateTransaksi->t_kredit = 0;
        $modelUpdateTransaksi->t_keterangan = $_POST['Transaksi']['t_keterangan_cash_flow'];
        $modelUpdateTransaksi->t_tanggal_update = date("Y-m-d H:i:s");

        
        if($modelUpdateTransaksi->save())
            $this->redirect(array('/Report/Keuangan'));
    	
    }

	public function actionPengeluaran()
	{
		$model=new Transaksi;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Transaksi']))
		{
			
			$model->attributes=$_POST['Transaksi'];
			$model->t_pegawai = Yii::app()->user->getState('idUser');
			$model->t_tanggal = date('Y-m-d',strtotime($_POST['Transaksi']['t_tanggal']));
			$model->t_debit = 0;
			$model->t_kredit = $_POST['Transaksi']['t_nominal'];
			$model->t_tanggal_insert = date("Y-m-d H:i:s");
			$model->t_tanggal_update = date("Y-m-d H:i:s");

			$gbr = $_FILES['t_bukti'];
            if($gbr['size']>0){
                $ext = pathinfo($gbr["name"], PATHINFO_EXTENSION);
                $file_name = md5(date("Y-m-d H:i:s").$gbr["name"]).'.'.$ext;
                move_uploaded_file($gbr['tmp_name'], 'bukti/'.$file_name);
                $model->t_bukti = $file_name;
            }

			if(isset($_POST['Transaksi']['t_sppd'])){
				$model->t_sppd = 1;
			}

			if($model->save())
				$this->redirect(array('Pengeluaran'));
		}

		$this->render('pengeluaran',array(
			'model'=>$model,
		));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Transaksi;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Transaksi']))
		{
			$model->attributes=$_POST['Transaksi'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->t_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Transaksi']))
		{
			$model->attributes=$_POST['Transaksi'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->t_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Transaksi');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Transaksi('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Transaksi']))
			$model->attributes=$_GET['Transaksi'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Transaksi the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Transaksi::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Transaksi $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='transaksi-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
