<?php

class KonfirmasiController extends Controller
{
	public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('Sppd','SppdDisetujui','SppdDibatalkan','RealisasiSPPD','DetailRealisasi','KonfirmasiRealisasi','Lembur','LemburDisetujui','LemburDibatalkan'),
                'expression'=>"Yii::app()->controller->isValidationUser()",
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
                'deniedCallback' => array($this, 'redirecting'),
            ),
        );
    }

    function isValidationUser() {
        if(Yii::app()->user->getState('_level')=='admin' || Yii::app()->user->getState('_level')=='koordinator')
            return true;
        return false;
	}

    public function redirecting(){
        $this->redirect(array('/Login'));
    }

	public function actionSppd()
	{
		$modelSNS = new Sppd();

		$dataBelumKonfirmasi = $modelSNS->getSppdBelumKonfirmasiAdmin()->getData();
		$dataSudahKonfirmasi = $modelSNS->getSppdSudahKonfirmasiAdmin()->getData();

		$this->render('sppd', array(
			'dataBelumKonfirmasi'=>$dataBelumKonfirmasi,
			'dataSudahKonfirmasi'=>$dataSudahKonfirmasi,
		));

	}

	public function actionSppdDisetujui($id)
	{
		$modelSppd = new Sppd();

		//Start Proses Insert Ke Tabel Absensi
		$datasppd = $modelSppd->getDatasppd($id);

		$idPegawai = $datasppd['s_pegawai'];
		$tanggal = $datasppd['s_tanggal'];
		$alasan = $datasppd['s_alasan'];

		$expldTanggal = explode(',', $tanggal);
		for ($i=0; $i < count($expldTanggal); $i++) { 
			$detail = $expldTanggal[$i];
			$expldDetail = explode('-', $detail);

			$date = str_replace('/', '-', $expldDetail[0]);
			$endDate = str_replace('/', '-', $expldDetail[1]);

			$date = date('Y-m-d', strtotime($date));
			$endDate = date('Y-m-d', strtotime($endDate));

			$begin = new DateTime( date('Y-m-d',strtotime($date)) );
			$end = new DateTime( date('Y-m-d',strtotime($endDate. ' +1 day')) );

			$interval = DateInterval::createFromDateString('1 day');
			$period = new DatePeriod($begin, $interval, $end);

			$aryRange=array();

			foreach ( $period as $dt ){
				$dataTanggal =  $dt->format( "Y-m-d" );
				
				$modelRealisasiHeader = new RealisasiSppdHeader();
				$modelRealisasiHeader->rsh_sppd = $id;
				$modelRealisasiHeader->rsh_tanggal = $dataTanggal;
				$modelRealisasiHeader->rsh_total = 0;
				$modelRealisasiHeader->rsh_status = 0;
			    
			    $modelRealisasiHeader->save();
			}
		}
		//Start Proses Update Status Cuti
		$modelSppd->updateDisetujui($id);
		//End 

		$this->redirect(array('sppd'));
	}

	public function actionSppdDibatalkan()
	{
		$modelSppd = new Sppd();

		$s_id = $_POST['s_id'];
		$s_keterangan = $_POST['s_total_hari'];

		$modelSppd->updateDibatalkan($s_id,$s_keterangan);
	}

	public function actionRealisasiSPPD()
	{
		$model = new RealisasiSppdHeader();
        $this->render('realisasiSppd',array(
            'dataSPPD' => $model->getDataKonfirmasiRealisasiSPPD()->getData(),
        ));
	}

	public function actionDetailRealisasi()
    {
        $model = new RealisasiSppd();
        echo json_encode($model->detailRealisasi($_POST['rs_header']));
    }

    public function actionKonfirmasiRealisasi()
    {
    	$model = new RealisasiSppd();
    	$modelHeader = new RealisasiSppdHeader();

        $rs_header = $_POST['rs_header'];
        $transport = $_POST['transport'];
        $file_transport = "";
        if(isset($_POST['file_transport']))
        	$file_transport = $_POST['file_transport'];

        $penginapan = $_POST['penginapan'];
        $file_penginapan = "";
        if(isset($_POST['file_penginapan']))
        	$file_penginapan = $_POST['file_penginapan'];

        $darike = $_POST['darike'];
        $makan = $_POST['makan'];
        $saku = $_POST['saku'];

        $total_sppd = 0;

        $model->deleteAllRecord($rs_header);

        if($transport != 0){
        	$total_sppd += $transport;
        	$model->insertRealisasi($rs_header,$transport,"transport",$file_transport);
        }

    	if($penginapan != 0){
    		$total_sppd += $penginapan;
    		$model->insertRealisasi($rs_header,$penginapan,"penginapan",$file_penginapan);
    	}
    		
    	if($darike != 0){
    		$total_sppd += $darike;
    		$model->insertRealisasi($rs_header,$darike,"dari dan ke","");
    	}
    		
    	if($makan != 0){
    		$total_sppd += $makan;
    		$model->insertRealisasi($rs_header,$makan,"uang makan","");
    	}
    		
    	if($saku != 0){
    		$total_sppd += $saku;
    		$model->insertRealisasi($rs_header,$saku,"uang saku","");
    	}
        $modelHeader->updateTotalSPPD($rs_header,$total_sppd);
    }

	public function actionLembur()
	{
		$model = new Lembur();

		$dataBelumKonfirmasi = $model->getLemburBelumKonfirmasiAdmin()->getData();
		$dataSudahKonfirmasi = $model->getLemburSudahKonfirmasiAdmin()->getData();

		$this->render('lembur', array(
				'dataBelumKonfirmasi'=>$dataBelumKonfirmasi,
				'dataSudahKonfirmasi'=>$dataSudahKonfirmasi,
			));

		
	}

	public function actionLemburDisetujui($id)
	{
		$modelLembur = new Lembur();

		
		//Start Proses Update Status Lembur
		$modelLembur->updateDisetujui($id);
		//End 

		$this->redirect(array('Lembur'));
	}

	public function actionLemburDibatalkan()
	{
		$modelLembur = new Lembur();

		$s_id = $_POST['s_id'];
		$s_keterangan = $_POST['s_total_hari'];

		$modelLembur->updateDibatalkan($s_id,$s_keterangan);
	}

}