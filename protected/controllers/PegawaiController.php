<?php

class PegawaiController extends Controller
{
	public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
        	array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('Setting','UpdatePassword'),
				'users'=>array('*'),
			),
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('Create','Update','View','Admin','Reset','Delete','Gaji','AddSalary','SearchBy'),
                'expression'=>"Yii::app()->user->getState('_level')=='admin'",
            ),
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('SearchBy'),
                'expression'=>"Yii::app()->user->getState('_level')=='koordinator'",
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
                'deniedCallback' => array($this, 'redirecting'),
            ),
        );
    }

    public function redirecting(){
        $this->redirect(array('/Login'));
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionAddSalary($id)
	{
		$modelPegawai=$this->loadModel($id);
		$modelGaji= new Gaji();
		$model=new Gaji('search');
		$model->g_pegawai = $id;
		if(isset($_POST['Gaji']))
		{
			$dataGaji = $_POST['Gaji']['gaji'];
			//Start Kalkulasi Gaji Per Jam
			// Gaji Per Jam = THP - 25% * 1/173
			$perhitunganPertama = (int)((int)$dataGaji*(25/100));
			$perhitunganKedua = ($perhitunganPertama - (int)$dataGaji);
			$perhitunganKetiga = (int)($perhitunganKedua/173);
			//$model->p_gaji_perjam = (int)((int)$_POST['Pegawai']['p_gaji']/173);
			$dataGajiPerjam = abs($perhitunganKetiga);
			//End 

			$modelGaji->g_pegawai = $id;
			$modelGaji->g_tanggal = date('Y-m-d',strtotime($_POST['Gaji']['tanggal']));
			$modelGaji->g_gaji = $dataGaji;
			$modelGaji->g_gaji_perjam = $dataGajiPerjam;
			$modelGaji->g_tanggal_insert = date("Y-m-d H:i:s");
			$modelGaji->save();
			// var_dump($_POST['Gaji']['tanggal']);
			// var_dump($_POST['Gaji']['gaji']);
			// var_dump($modelGaji->save());
			// var_dump($dataGaji);
			// var_dump($dataGajiPerjam);
			//if($model->save())
			$this->redirect(array('gaji','id'=>$id));
		}else{
			
			$model->unsetAttributes();  // clear any default values
			if(isset($_GET['Gaji']))
				$model->attributes=$_GET['Gaji'];

			$this->render('addSalary',array(
				'model'=>$model,
				'modelPegawai'=>$modelPegawai,
				
			));
		}
	}

	public function actionGaji($id)
	{
		
		$model=new Gaji('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Gaji']))
			$model->attributes=$_GET['Gaji'];

		$this->render('gaji',array(
			'model'=>$model,
			'id'=>$id,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Pegawai;
		$modelPegawai = new Pegawai();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pegawai']))
		{
			$model->attributes=$_POST['Pegawai'];
			$model->p_password = sha1($_POST['Pegawai']['p_username']);

			//Start Create NIP
			$expldTglMasuk = explode('-', $model->p_tgl_masuk);
			$expldTglLahir = explode('-', $model->p_tgl_lahir);

			$tahunBulanMasukNIP = substr($expldTglMasuk[2],-2).$expldTglMasuk[1];
			
			$lastNIP = $modelPegawai->getLastNIP();

			if($_POST['Pegawai']['p_jk'] == 'L'){
				$jkNIP = 1;
			}else{
				$jkNIP = 2;
			}

			$bulanTanggalLahirNIP = $expldTglLahir[1].$expldTglLahir[0];

			$fixNIP = $tahunBulanMasukNIP.$lastNIP['Nomer'].$jkNIP.$bulanTanggalLahirNIP;
			//End Create NIP
			
			//Start Kalkulasi Gaji Per Jam
			// Gaji Per Jam = THP - 25% * 1/173
			$perhitunganPertama = (int)((int)$_POST['Pegawai']['p_gaji']*(25/100));
			$perhitunganKedua = ($perhitunganPertama - (int)$_POST['Pegawai']['p_gaji']);
			$perhitunganKetiga = (int)($perhitunganKedua/173);
			//$model->p_gaji_perjam = (int)((int)$_POST['Pegawai']['p_gaji']/173);
			$model->p_gaji_perjam = abs($perhitunganKetiga);
			//End 

			$model->p_nip = $fixNIP;
			$model->p_tgl_masuk = date('Y-m-d',strtotime($model->p_tgl_masuk));
			$model->p_tgl_lahir = date('Y-m-d',strtotime($model->p_tgl_lahir));

			$model->p_koordinator = $_POST['Pegawai']['p_koordinator'];
			
			if($model->save())
				$this->redirect(array('view','id'=>$model->p_id));
		}

		$this->render('create',array(
			'model'=>$model
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pegawai']))
		{
			$model->attributes=$_POST['Pegawai'];

			//Start Kalkulasi Gaji Per Jam
			// Gaji Per Jam = THP - 25% * 1/173
			$perhitunganPertama = (int)((int)$_POST['Pegawai']['p_gaji']*(25/100));
			$perhitunganKedua = ($perhitunganPertama - (int)$_POST['Pegawai']['p_gaji']);
			$perhitunganKetiga = (int)($perhitunganKedua/173);
			//$model->p_gaji_perjam = (int)((int)$_POST['Pegawai']['p_gaji']/173);
			$model->p_gaji_perjam = abs($perhitunganKetiga);
			//End 

			$model->p_password = $model->p_password;
			$model->p_koordinator = $_POST['Pegawai']['p_koordinator'];
			$model->p_tgl_masuk = date('Y-m-d',strtotime($model->p_tgl_masuk));
			$model->p_tgl_lahir = date('Y-m-d',strtotime($model->p_tgl_lahir));
			if($model->save())
				$this->redirect(array('view','id'=>$model->p_id));
		}

		$model->p_tgl_masuk = date('d-m-Y',strtotime($model->p_tgl_masuk));
		$model->p_tgl_lahir = date('d-m-Y',strtotime($model->p_tgl_lahir));
		$this->render('update',array(
			'model'=>$model
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionReset($id)
	{
		$model=$this->loadModel($id);
		$username = $model->p_username;
		$newPassword = sha1($username);
		$modelPegawai = new Pegawai();
		$modelPegawai->updatePassword($id,$newPassword);
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Pegawai');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Pegawai('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Pegawai']))
			$model->attributes=$_GET['Pegawai'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionSetting()
	{
		$modelGaji = new Gaji();
		$dataGaji = $modelGaji->getDataGaji(Yii::app()->user->getState('idUser'));
		$model=$this->loadModel(Yii::app()->user->getState('idUser'));
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pegawai']))
		{
			$model->attributes=$_POST['Pegawai'];
			$model->p_nip = $model->p_nip;
			$model->p_jabatan = $model->p_jabatan;
			$model->p_tgl_masuk = $model->p_tgl_masuk;
			$model->p_gaji = $model->p_gaji;
			$model->p_gaji_perjam = $model->p_gaji_perjam;
			$model->p_level = $model->p_level;
			$model->p_status = $model->p_status;
			$model->p_status_pegawai = $model->p_status_pegawai;
			$model->p_tgl_lahir = date('Y-m-d',strtotime($model->p_tgl_lahir));
			if($model->save())
				$this->redirect(array('setting'));
		}

		$model->p_tgl_masuk = date('d-m-Y',strtotime($model->p_tgl_masuk));
		$model->p_tgl_lahir = date('d-m-Y',strtotime($model->p_tgl_lahir));
		$this->render('setting',array(
			'model'=>$model,
			'dataGaji'=>$dataGaji,
		));
	}

	public function actionUpdatePassword()
	{
		$idUser = $_POST['idUser'];
		$lastPassword = sha1($_POST['lastPassword']);
		$newPassword = sha1($_POST['newPassword']);

		$model=$this->loadModel($idUser);

		if($lastPassword == $model->p_password){
			$model->updatePassword($idUser,$newPassword);
			echo "1";
		}else{
			echo "0";
		}
	}

	public function actionSearchBy()
	{
		$field = $_POST['field'];
		$value = $_POST['value'];
		$modelPegawai = new Pegawai();
		$model = $modelPegawai->SearchBy($field,$value);

		$this->render('searchBy',array(
			'model'=>$model,
		));
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Pegawai the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Pegawai::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Pegawai $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pegawai-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
