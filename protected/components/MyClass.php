<?php


class MyClass extends CApplicationComponent {
	 
	 public function get_my_info() {
	 
	     //your code here
	     //your code here
	 
	     return 0;
	 }

	public  function FormatRupiah($parm){
	    return 'Rp. ' . number_format( floatval($parm), 0 , '' , '.' ) . ',00';
	}

	public  function FormatRupiahShort($parm){
	    return number_format( $parm, 0 , '' , '.' );
	}

	public  function FormatHariIndonesia($parm){
	    
		$array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat", "Sabtu","Minggu");
		$dataHari = date('N',strtotime($parm));
		return $array_hari[$dataHari];

	}

	public function FormatTanggalIndonesia($parm){

		$array_bulan = array(1=>"Januari","Februari","Maret", "April", "Mei", "Juni","Juli","Agustus","September","Oktober", "November","Desember");
		$dataBulan = date('n',strtotime($parm));
		return date('d',strtotime($parm))." ".$array_bulan[$dataBulan]." ".date('Y',strtotime($parm));

	}

	public function FormatTanggalHariIndonesia($parm){

		if($parm == '0000-00-00 00:00:00'){
			return "Belum Check Out";
		}
		$array_bulan = array(1=>"Januari","Februari","Maret", "April", "Mei", "Juni","Juli","Agustus","September","Oktober", "November","Desember");
		$dataBulan = date('n',strtotime($parm));

		$array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat", "Sabtu","Minggu");
		$dataHari = date('N',strtotime($parm));

		return $array_hari[$dataHari].", ".date('d',strtotime($parm))." ".$array_bulan[$dataBulan]." ".date('Y',strtotime($parm));

	}

	public function FormatTanggalHariWaktuIndonesia($parm){

		if($parm == '0000-00-00 00:00:00'){
			return "-";
		}
		$array_bulan = array(1=>"Januari","Februari","Maret", "April", "Mei", "Juni","Juli","Agustus","September","Oktober", "November","Desember");
		$dataBulan = date('n',strtotime($parm));

		$array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat", "Sabtu","Minggu");
		$dataHari = date('N',strtotime($parm));

		$dataWaktu = date('H:i',strtotime($parm));

		return $array_hari[$dataHari].", ".date('d',strtotime($parm))." ".$array_bulan[$dataBulan]." ".date('Y',strtotime($parm))." ".$dataWaktu;

	}

	public function FormatTanggalWaktuIndonesia($parm){

		if($parm == '0000-00-00 00:00:00'){
			return "Belum Check Out";
		}
		$array_bulan = array(1=>"Januari","Februari","Maret", "April", "Mei", "Juni","Juli","Agustus","September","Oktober", "November","Desember");
		$dataBulan = date('n',strtotime($parm));

		$array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat", "Sabtu","Minggu");
		$dataHari = date('N',strtotime($parm));

		$dataWaktu = date('H:i',strtotime($parm));

		return date('d',strtotime($parm))." ".$array_bulan[$dataBulan]." ".date('Y',strtotime($parm))." ".$dataWaktu;

	}

	public  function FormatWaktuIndonesia($parm){
	    if($parm == '0000-00-00 00:00:00'){
			return "-";
		}

		return date('H:i',strtotime($parm));

	}

	public function HitungLemburNormal($total,$upahPerJam){
		//Perhitungan untuk jam kerja tambahan
		$jamPertama = 1.5*$upahPerJam;
		$jamSisa = ($total-1)*2*$upahPerJam;

		$hasil = $jamPertama + $jamSisa;
		return $hasil;
	}
 	
 	public function HitungLemburHarian($total,$upahPerJam){
 		//Perhitungan untuk hari libur atau pengganti bagi yang shift
 		if($total<= 8){
 			$hasil = $total*2*$upahPerJam;
 		}else if($total <= 9){
 			$jamPertama =$total*2*$upahPerJam;
			$jamSisa = 3*$upahPerJam;
			$hasil = $jamPertama + $jamSisa;
 		}else if($total <= 11){
 			$jamPertama =$total*2*$upahPerJam;
			$jamKedua = 3*$upahPerJam;
			$jamSisa = ($total-9)*4*$upahPerJam;
			$hasil = $jamPertama + $jamKedua + $jamSisa;
 		}else{
 			$total = 11;
 			$jamPertama =$total*2*$upahPerJam;
			$jamKedua = 3*$upahPerJam;
			$jamSisa = ($total-9)*4*$upahPerJam;
			$hasil = $jamPertama + $jamKedua + $jamSisa;
 		}
 		return $hasil;
	}


	public function HitungInsentifLembur($total,$gaji){
 		
 		$perHari = ($gaji/23)*2;
 		$hasil = $perHari * $total;
 		return $hasil;
	}

	public function DiffDateForHour($date1,$date2){
		//Menghitung Selisih Jam
		$t1 = StrToTime ( $date1 );
		$t2 = StrToTime ( $date2);
		$diff = $t1 - $t2;
		$hours = $diff / ( 60 * 60 );
		return (int)abs($hours);
	}

}

?>