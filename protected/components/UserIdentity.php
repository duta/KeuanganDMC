<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{

    public $role;
    public $rolename;
    private $_id;

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        $user = Pegawai::model()->findByAttributes(array('p_username'=>$this->username));
        $pass = sha1($this->password);
        if (isset($user)){
            if($pass === $user->p_password){
                $this->setState('id', $user->p_id);
                $this->setState('idUser', $user->p_id);
                $this->setState('namaUser', $user->p_nama_lengkap);
                $this->setState('jabatanUser', $user->p_jabatan);
                $this->setState('statusUser', $user->p_status_pegawai);
                $this->setState('_level', $user->p_level);
                $this->setState('role', $user->p_level);
                $this->errorCode=self::ERROR_NONE;
            }else{
                $this->errorCode=self::ERROR_PASSWORD_INVALID;
            }
        } else{
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        }
        return !$this->errorCode;

    }

    public function getId()
    {
        return $this->_id;
    }

}
