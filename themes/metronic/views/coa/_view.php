<?php
/* @var $this CoaController */
/* @var $data Coa */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('c_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->c_id), array('view', 'id'=>$data->c_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('c_nama')); ?>:</b>
	<?php echo CHtml::encode($data->c_nama); ?>
	<br />


</div>