<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
Lihat Profil
</h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="icon-user"></i>
			<a href="">Lihat Profil</a>
		</li>
	</ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row margin-top-20">
	<div class="col-md-12">
		<!-- BEGIN PROFILE SIDEBAR -->
		<div class="profile-sidebar">
			<!-- PORTLET MAIN -->
			<div class="portlet light profile-sidebar-portlet">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
					<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/custom/img/avatar.png" class="img-responsive" alt="">
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">
						 <?=$model->p_nama_lengkap;?>
					</div>
					<div class="profile-usertitle-job">
						 <?=ucfirst($model->p_level);?>
					</div>
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<br>
			</div>
			<!-- END PORTLET MAIN -->
			<!-- PORTLET MAIN -->
			<!--div class="portlet light">
				<div class="row list-separated profile-stat">
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div class="uppercase profile-stat-title">
							 37
						</div>
						<div class="uppercase profile-stat-text">
							 Projects
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div class="uppercase profile-stat-title">
							 51
						</div>
						<div class="uppercase profile-stat-text">
							 Tasks
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div class="uppercase profile-stat-title">
							 61
						</div>
						<div class="uppercase profile-stat-text">
							 Uploads
						</div>
					</div>
				</div>
			</div>
			<!-- END PORTLET MAIN -->
		</div>
		<!-- END BEGIN PROFILE SIDEBAR -->
		<!-- BEGIN PROFILE CONTENT -->
		<div class="profile-content">
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light">
						<div class="portlet-title tabbable-line">
							<div class="caption caption-md">
								<i class="icon-globe theme-font hide"></i>
								<!--span class="caption-subject font-blue-madison bold uppercase">Profile Account</span-->
							</div>
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#tab_1_1" data-toggle="tab">Data Pribadi</a>
								</li>
								<!-- <li>
									<a href="#tab_1_2" data-toggle="tab">Foto Profil</a>
								</li> -->
								<li>
									<a href="#tab_1_3" data-toggle="tab">Ganti Kata Sandi</a>
								</li>
							</ul>
						</div>
						<div class="portlet-body">
							<div class="tab-content">
								<!-- PERSONAL INFO TAB -->
								<div class="tab-pane active" id="tab_1_1">
									<div class="portlet-body form">
										<form class="form-horizontal" role="form">
											<div class="row">
												<div class="col-md-12 ">
													<div class="row">
														<div class="col-md-6">
															<div class="form-body">
																<div class="form-group">
																	<label class="control-label col-md-3 bold">Username:</label>
																	<div class="col-md-9">
																		<p class="form-control-static">
																			 <?=$model->p_username;?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-body">
																<div class="form-group">
																	<label class="control-label col-md-3 bold">Status Pegawai:</label>
																	<div class="col-md-9">
																		<p class="form-control-static">
																			 <?php
																			 	if($model->p_status_pegawai == 2){
																					echo "Shift";
																			 	}else{
																					echo "Non Shift";
																				}
																			 ?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-6">
															<div class="form-body">
																<div class="form-group">
																	<label class="control-label col-md-3 bold">NIP:</label>
																	<div class="col-md-9">
																		<p class="form-control-static">
																			 <?=$model->p_nip;?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-body">
																<div class="form-group">
																	<label class="control-label col-md-3 bold">Nama Lengkap:</label>
																	<div class="col-md-9">
																		<p class="form-control-static">
																			 <?=$model->p_nama_lengkap;?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-6">
															<div class="form-body">
																<div class="form-group">
																	<label class="control-label col-md-3 bold">Jabatan:</label>
																	<div class="col-md-9">
																		<p class="form-control-static">
																			 <?=$model->jabatan->j_nama;?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-body">
																<div class="form-group">
																	<label class="control-label col-md-3 bold">Jenis Kelamin:</label>
																	<div class="col-md-9">
																		<p class="form-control-static">
																			 <?=$model->p_jk;?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-6">
															<div class="form-body">
																<div class="form-group">
																	<label class="control-label col-md-3 bold">Tempat Lahir:</label>
																	<div class="col-md-9">
																		<p class="form-control-static">
																			 <?=$model->p_tempat_lahir;?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-body">
																<div class="form-group">
																	<label class="control-label col-md-3 bold">Tanggal Lahir:</label>
																	<div class="col-md-9">
																		<p class="form-control-static">
																			 <?=date('d-m-Y',strtotime($model->p_tgl_lahir));?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-6">
															<div class="form-body">
																<div class="form-group">
																	<label class="control-label col-md-3 bold">Tanggal Masuk:</label>
																	<div class="col-md-9">
																		<p class="form-control-static">
																			 <?=date('d-m-Y',strtotime($model->p_tgl_masuk));?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-body">
																<div class="form-group">
																	<label class="control-label col-md-3 bold">Alamat:</label>
																	<div class="col-md-9">
																		<p class="form-control-static">
																			 <?=$model->p_alamat;?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-6">
															<div class="form-body">
																<div class="form-group">
																	<label class="control-label col-md-3 bold">No Telp 1:</label>
																	<div class="col-md-9">
																		<p class="form-control-static">
																			 <?=$model->p_no_telp_1;?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-body">
																<div class="form-group">
																	<label class="control-label col-md-3 bold">No Telp 2:</label>
																	<div class="col-md-9">
																		<p class="form-control-static">
																			 <?=$model->p_no_telp_2;?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
													</div>

										          	<div class="row">
														<div class="col-md-6">
															<div class="form-body">
																<div class="form-group">
																	<label class="control-label col-md-3 bold">Email:</label>
																	<div class="col-md-9">
																		<p class="form-control-static">
																			 <?=$model->p_email;?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-body">
																<div class="form-group">
																	<label class="control-label col-md-3 bold">No Rekening:</label>
																	<div class="col-md-9">
																		<p class="form-control-static">
																			 <?=$model->p_norekening;?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-6">
															<div class="form-body">
																<div class="form-group">
																	<label class="control-label col-md-3 bold">Gaji:</label>
																	<div class="col-md-9">
																		<p class="form-control-static">
																			 <?=Yii::app()->myClass->FormatRupiah($model->p_gaji);?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-body">
																<div class="form-group">
																	<label class="control-label col-md-3 bold">Gaji Per Jam:</label>
																	<div class="col-md-9">
																		<p class="form-control-static">
																			 <?=Yii::app()->myClass->FormatRupiah($model->p_gaji_perjam);?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-6">
															<div class="form-body">
																<div class="form-group">
																	<label class="control-label col-md-3 bold">Level:</label>
																	<div class="col-md-9">
																		<p class="form-control-static">
																			 <?=ucfirst($model->p_level);?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-body">
																<div class="form-group">
																	<label class="control-label col-md-3 bold">Nama Koordinator:</label>
																	<div class="col-md-9">
																		<p class="form-control-static">
																			<?php echo (isset($model->koordinator) ? ucfirst($model->koordinator->p_nama_lengkap) : '');?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-body">
																<div class="form-group">
																	<label class="control-label col-md-3 bold">Status Aktif:</label>
																	<div class="col-md-9">
																		<p class="form-control-static">
																			 <?php
																			 	if($model->p_status == 1){
																					echo "Aktif";
																			 	}else{
																					echo "Tidak Aktif";
																				}
																			 ?>
																		</p>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
								<!-- END PERSONAL INFO TAB -->
								<!-- CHANGE AVATAR TAB -->
								<div class="tab-pane" id="tab_1_2">
									<form action="#" role="form">
										<div class="form-group">
											<div class="fileinput fileinput-new" data-provides="fileinput">
												<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
													<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
												</div>
												<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
												</div>
												<div>
													<span class="btn default btn-file">
													<span class="fileinput-new">
													Select image </span>
													<span class="fileinput-exists">
													Change </span>
													<input type="file" name="...">
													</span>
												</div>
											</div>
										</div>
										<div class="margin-top-10">
											<a href="javascript:;" class="btn green-haze">
											Simpan</a>
											<a href="javascript:;" class="btn default">
											Cancel </a>
										</div>
									</form>
								</div>
								<!-- END CHANGE AVATAR TAB -->
								<!-- CHANGE PASSWORD TAB -->
								<div class="tab-pane" id="tab_1_3">
									<div class="alert alert-success display-hide alert-password">
										<button class="close" data-close="alert"></button>
										<span id="textAlertPassword"></span>
									</div>
									<form action="#">
										<div class="form-group">
											<label class="control-label">Kata Sandi Lama</label>
											<input type="password" class="form-control" id="lastPassword"/>
										</div>
										<div class="form-group">
											<label class="control-label">Kata Sandi Baru</label>
											<input type="password" class="form-control" id="newPassword"/>
										</div>
										<div class="form-group">
											<label class="control-label">Ulangi Kata Sandi Baru</label>
											<input type="password" class="form-control" id="newRetypePassword"/>
										</div>
										<div class="margin-top-10">
											<a href="javascript:;" class="btn green-haze" onclick="updatePassword();">
											Simpan</a>
											<a href="javascript:;" class="btn default">
											Batal </a>
										</div>
									</form>
								</div>
								<!-- END CHANGE PASSWORD TAB -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END PROFILE CONTENT -->
	</div>
</div>
<!-- END PAGE CONTENT-->
<script type="text/javascript">
	function updatePassword(){
		var idUser = <?=$model->p_id;?>;
		var lastPassword = document.getElementById('lastPassword').value;
		var newPassword = document.getElementById('newPassword').value;
		var newRetypePassword = document.getElementById('newRetypePassword').value;

		if(lastPassword == "" || newPassword == "" || newRetypePassword == ""){
			$("#textAlertPassword").html("Lengkapi Data Inputan !");
			$(".alert-password").addClass( "alert-danger" );
			$(".alert-password").removeClass( "alert-success" );
			$(".alert-password").show();
			return false;
		}

		if(newPassword != newRetypePassword){
			$("#textAlertPassword").html("Kata Sandi Tidak Sama !");
			$(".alert-password").addClass( "alert-danger" );
			$(".alert-password").removeClass( "alert-success" );
			$(".alert-password").show();
			return false;
		}
		$.ajax({
	        url:"<?=$this->createUrl('Pegawai/UpdatePassword');?>",
	        type: 'POST',
	        data:{
	          "idUser" : idUser,
	          "lastPassword" : lastPassword,
	          "newPassword" : newPassword
	        },
	        success: function(data) {
	        	if(data == 1){
	        		document.getElementById('lastPassword').value = "";
	        		document.getElementById('newPassword').value = "";
	        		document.getElementById('newRetypePassword').value = "";

	        		$("#textAlertPassword").html("Berhasil Merubah Kata Sandi");
					$(".alert-password").addClass( "alert-success" );
					$(".alert-password").removeClass( "alert-danger" );
					$(".alert-password").show();

	        	}else{
	        		$("#textAlertPassword").html("Kata Sandi Lama Anda Salah !");
	        		$(".alert-password").addClass( "alert-danger" );
					$(".alert-password").removeClass( "alert-success" );
					$(".alert-password").show();

	        	}

	    	},cache: false
		});
	}


</script>
