<h3 class="page-title">
Gaji Data Pegawai
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="#">Master</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/admin'); ?>">Data Pegawai</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Gaji</a>
    </li>
  </ul>
</div>

<script type="text/javascript">
$(document).ready(function(){

  $('table.items').addClass('table-bordered table-striped table-condensed cf');
  $('div.summary').hide();
  $('th[id=gaji-grid_c0]').css('width','5%');

});
</script>

<div class="row ">
  <div class="col-md-12">
    <!-- BEGIN SAMPLE FORM PORTLET-->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="actions">
          <a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/addSalary/'.$id); ?>" class="btn btn-default btn-sm">
          <i class="fa fa-plus"></i> Tambah </a>
        </div>
      </div>
      <div class="portlet-body">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
          'id'=>'gaji-grid',
          'dataProvider'=>$model->search($id),
          'template' => "{items}{pager}",
          'enablePagination' => true,
          'columns'=>array(
            array('name' => 'number', 'header' => 'No.','value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1', 'htmlOptions' => array('style' => 'text-align:center')),
            array('name' => 'g_tanggal', 'value' => 'formatTanggal($data[\'g_tanggal\'])'),
            array('name' => 'g_pegawai','value' => '$data[\'pegawai\'][\'p_nama_lengkap\']','filter'=>false),
            array('name' => 'g_gaji', 'value' => 'formatRupiah($data[\'g_gaji\'])', 'htmlOptions' => array('style' => 'text-align:right')),
            array('name' => 'g_gaji_perjam', 'value' => 'formatRupiah($data[\'g_gaji_perjam\'])', 'htmlOptions' => array('style' => 'text-align:right')),
          ),
        ));


        function formatRupiah($parm){
          return Yii::app()->myClass->FormatRupiah($parm);
        } 

        function formatTanggal($parm){
          return Yii::app()->myClass->FormatTanggalIndonesia($parm);
        } 
        ?>
      </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->
  </div>
</div>
