<h3 class="page-title">
Tambah Data Pegawai
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="#">Master</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/admin'); ?>">Data Pegawai</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Tambah Data</a>
    </li>
  </ul>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
