<table class="table table-striped table-bordered table-hover" id="check1">
  <thead>
  <tr>
    <th>
       Pegawai
    </th>
    <th>
      Jenis Lembur
    </th>
    <th>
       Hari, Tanggal
    </th>
    <th>
       Jam Mulai
    </th>
    <th>
       Total Lembur
    </th>
    <th>
       Keterangan
    </th>
    <th>
       Proyek
    </th>
    <th>Gaji Per Jam</th>
    <th>Sub Total</th>
    <th style="display:none;">
      
    </th>
    <th class="text-center">
      Select All <br />
      <input type="checkbox" id="selectAll">
    </th>
  </tr>
  </thead>
  <tbody>
  <?php 
  $total = 0;
  foreach($dataLembur as $val) {   

    if($val['l_jenis'] == 1){

      $biayaLembur = Yii::app()->myClass->HitungLemburNormal($val['l_total_jam'],$val['gaji_perjam']);
      
    }else{

      $biayaLembur =  Yii::app()->myClass->HitungLemburHarian($val['l_total_jam'],$val['gaji_perjam']);
      
    }

    
      $total = $total + $biayaLembur;
    
  ?>
    <tr>
      <td>
        <?=$val['pegawai']?>
      </td>
      <td>
        <?php 
          if($val['l_jenis'] == 1){
              echo "Hari Biasa";
          }else{
            echo "Hari Libur";
          }
        ?>
      </td>
      <td>
        <?=Yii::app()->myClass->FormatTanggalHariIndonesia($val['l_tanggal'])?>
      </td>
      <td>
        <?=date('H:i',strtotime($val['l_waktu']))?>
      </td>
      <td align="center">
        <?=$val['l_total_jam']?> jam
      </td>
      <td>
        <?=$val['l_keterangan']?>
      </td>
      <td>
        <?=$val['p_nama']?>
      </td>
      <td align="right">
        <?=Yii::app()->myClass->FormatRupiah($val['gaji_perjam'])?>
      </td>
      <td align="right">
        <?=Yii::app()->myClass->FormatRupiah($biayaLembur)?>
      </td>
      <td style="display:none;">
        <?=$val['l_id']?>
      </td>
      <td align="center">
        <input type="checkbox" name="konfirmList">
      </td>
    </tr>

  <?php } ?>
  <tr>
      <td colspan="8" class="bold" align="right">Total</td>
      <td class="bold" align="right"><?=Yii::app()->myClass->FormatRupiah($total)?></td>
      <td></td>
    </tr>
  </tbody>
</table>

<div class="row">
  <div class="col-md-12" style="margin-bottom:10px;">
    <span class="pull-right">
      <?php if($status == 0){ ?>
      <button class="btn blue actionForm" id="setuju"><i class="fa fa-check"></i> Dibayar</button>
      <?php } ?>
      <button class="btn" onClick="window.location.reload()">Batal</button>
    </span>
  </div>
</div>


<script type="text/javascript">
  $(document).ready(function(){
    $('#selectAll').click(function() {
      var status = this.checked;
      $('#check1 tbody tr').find('td').each(function() {
        $(":checkbox").prop('checked',status);
      });
    });

    $(".actionForm").click(function(){
      var oData = {
        l_id:[],
        status:-1
      };
      var idx = 0;
      $('#check1 tbody').find('input[type="checkbox"]:checked').each(function () {
           var l_id = parseInt($(this).parent().prev().html());
           oData.l_id[idx] = l_id;
           idx++;
        });
        var word="";
        if(this.id == "setuju"){
          word = "menyetujui";
          oData.status = 1;
        }else if(this.id == "tolak"){
          word = "menolak";
          oData.status = 2;
        }

        if(Object.keys(oData.l_id).length > 0){
          if (confirm('Anda yakin ingin '+word+" "+Object.keys(oData.l_id).length+" lembur ?")) {
            Metronic.blockUI({
                  boxed: true
              });
            $.ajax({
                  url: "<?php echo $this->createUrl('Operasional/SaveDataLembur') ?>",
                  type: 'POST',
                  data: oData,
                  success: function(data) {
                    location.reload();
                    Metronic.unblockUI();
                  },
                  error: function(data) {
                      alert("Error!");
                      Metronic.unblockUI();
                  }   
              });
          }
      }
    });

    $(".spinner-up").click(function(){
      var nmr = $(this).parent().parent().find('input').val();
      nmr++;
      $(this).parent().parent().find('input').val(nmr);
    });

    $(".spinner-down").click(function(){
      var nmr = $(this).parent().parent().find('input').val();
      nmr--;
      if(nmr==0)
        nmr=1;
      $(this).parent().parent().find('input').val(nmr);
    });
  });
    
</script>