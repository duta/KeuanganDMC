<h3 class="page-title">
Operasional Cash Flow
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="#">Operasional</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Cash Flow</a>
    </li>
  </ul>
</div>

<div class="row">
	<div class="col-md-12 ">
		<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					Form
				</div>
				<div class="tools">
					<a title="" data-original-title="" href="" class="collapse">
					</a>
				</div>
			</div>
			<div class="portlet-body form">
				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'transaksi-form',
					// Please note: When you enable ajax validation, make sure the corresponding
					// controller action is handling ajax validation correctly.
					// There is a call to performAjaxValidation() commented in generated controller code.
					// See class documentation of CActiveForm for details on this.
					'enableAjaxValidation'=>false,
				)); ?>
					<div class="form-body">

						<div class="row">
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label>Tanggal</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-calendar"></i>
											</span>
											<input name="Transaksi[t_tanggal]" class="form-control date-picker" placeholder="Tanggal" type="text" data-date-format="dd-mm-yyyy" readonly>
											<input type="hidden" value="0" name="Transaksi[t_proyek]">
										</div>
									</div>
								</div>
							</div>
							<!--div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label>Proyek</label>
										<div class="input-group">
											<?php 
											/*$criteria = new CDbCriteria();
											$criteria->addCondition("p_status=1");
											echo $form->dropDownList($model,'t_proyek',CHtml::listData(Proyek::model()->findAll($criteria),'p_id','p_nama'),array('class'=>'form-control  input-large'));*/ ?>

											
										</div>
									</div>
								</div>
							</div-->
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label>Pegawai</label>
										<div class="input-group">
											<?php 
											$criteria = new CDbCriteria();
											$criteria->addCondition("p_level='pegawai'");
											$criteria->addCondition("p_status=1");
											echo $form->dropDownList($model,'t_pegawai',CHtml::listData(Pegawai::model()->findAll($criteria),'p_id','p_nama_lengkap'),array('class'=>'form-control  input-large')); ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label>Nominal</label>
										<div class="input-group">
											<input type="text" class="form-control  input-large" name="Transaksi[t_nominal]">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-body">
									<div class="form-group">
										<label>Keterangan</label>
										<textarea class="form-control" rows="3" name="Transaksi[t_keterangan]" placeholder="Keterangan"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-actions">
						<?php echo CHtml::submitButton('Simpan',array('class'=> 'btn blue')); ?>
					</div>
				<?php $this->endWidget(); ?>
			</div>
		</div>
		<!-- END SAMPLE FORM PORTLET-->
	</div>
</div>

<script>
	var textAreas = document.getElementsByTagName('textarea');
	Array.prototype.forEach.call(textAreas, function(elem) {
	    elem.placeholder = elem.placeholder.replace(/\\n/g, '\n');
	});
</script>