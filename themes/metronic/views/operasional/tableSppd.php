<table class="table table-striped table-bordered table-hover" id="check1">
  <thead>
  <tr>
    <th>
       Pegawai
    </th>
    <th>
       Tanggal Pengajuan
    </th>
    <th>
       Proyek
    </th>
    <th>
       Alasan
    </th>
    <th>
       Tanggal sppd
    </th>
    <th>
       Total Hari
    </th>
    <th>Insentif</th>
    <th style="display:none;">
      
    </th>
    <th class="text-center">
      Select All <br />
      <input type="checkbox" id="selectAll">
    </th>
  </tr>
  </thead>
  <tbody>
  <?php 
  $total = 0;
  foreach($dataSppd as $val) {   
    $insentif =Yii::app()->myClass->HitungInsentifLembur($val['s_total_hari'], $val['gaji']);
    $total = $total + $insentif;
  ?>
    <tr>
      <td>
        <?=$val['pegawai']?>
      </td>
      <td>
        <?=$val['s_tanggal_insert']?>
      </td>
      <td>
        <?=$val['p_nama']?>
      </td>
      <td>
        <?=$val['s_alasan']?>
      </td>
      <td>
      <?php 
      $exp1=$val['s_tanggal'];                    
      $expl1=explode(',',$val['s_tanggal']);
      for($i = 0; $i < count($expl1); $i++){
      //echo $expl1[$i];
      $detail = $expl1[$i];
      $expldDetail = explode('-', $detail);
      $perTanggal1 = date('d M Y',strtotime(str_replace('/','-',$expldDetail[0])));
      $perTanggal2 = date('d M Y',strtotime(str_replace('/','-',$expldDetail[1])));
      echo date('d M Y', strtotime(str_replace('/','-',$expldDetail[0])));
      echo " - ";
      echo date('d M Y', strtotime(str_replace('/','-',$expldDetail[1])));
      //echo "$expldDetail[1]";
      /*echo "$perTanggal1 - $perTanggal1";*/
      echo "<br/>";
      }                   
      ?>    
      </td>
      <td align="center">
         <?=$val['s_total_hari']?>
      </td>
      <td align="right">
        <?=Yii::app()->myClass->FormatRupiah($insentif)?>
      </td>
      <td style="display:none;">
        <?=$val['s_id']?>
      </td>
      <td align="center">
        <input type="checkbox" name="konfirmList">
      </td>
    </tr>
  <?php } ?>
  <tr>
      <td colspan="6" class="bold" align="right">Total</td>
      <td class="bold" align="right"><?=Yii::app()->myClass->FormatRupiah($total)?></td>
      <td></td>
    </tr>
  </tbody>
</table>

<div class="row">
  <div class="col-md-12" style="margin-bottom:10px;">
    <span class="pull-right">
      <?php if($status == 0){ ?>
      <button class="btn blue actionForm" id="setuju"><i class="fa fa-check"></i> Dibayar</button>
      <?php } ?>
      <button class="btn" onClick="window.location.reload()">Batal</button>
    </span>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $('#selectAll').click(function() {
      var status = this.checked;
      $('#check1 tbody tr').find('td').each(function() {
        $(":checkbox").prop('checked',status);
      });
    });

    $(".actionForm").click(function(){
      var oData = {
        s_id:[],
        status:-1
      };
      var idx = 0;
      $('#check1 tbody').find('input[type="checkbox"]:checked').each(function () {
           var s_id = parseInt($(this).parent().prev().html());
           oData.s_id[idx] = s_id;
           idx++;
        });
        var word="";
        if(this.id == "setuju"){
          word = "menyetujui";
          oData.status = 1;
        }else if(this.id == "tolak"){
          word = "menolak";
          oData.status = 2;
        }

        if(Object.keys(oData.s_id).length > 0){
          if (confirm('Anda yakin ingin '+word+" "+Object.keys(oData.s_id).length+" lembur ?")) {
            Metronic.blockUI({
                  boxed: true
              });
            $.ajax({
                  url: "<?php echo $this->createUrl('Operasional/SaveDataSppd') ?>",
                  type: 'POST',
                  data: oData,
                  success: function(data) {
                    location.reload();
                    Metronic.unblockUI();
                  },
                  error: function(data) {
                      alert("Error!");
                      Metronic.unblockUI();
                  }   
              });
          }
      }
    });

    $(".spinner-up").click(function(){
      var nmr = $(this).parent().parent().find('input').val();
      nmr++;
      $(this).parent().parent().find('input').val(nmr);
    });

    $(".spinner-down").click(function(){
      var nmr = $(this).parent().parent().find('input').val();
      nmr--;
      if(nmr==0)
        nmr=1;
      $(this).parent().parent().find('input').val(nmr);
    });
  });
    
</script>