<style type="text/css">
  table , td, th {
    border: 1px solid #E5E5E5;
    border-collapse: initial;
    width: 100%;
  }
  td, th {
    padding: 3px;
    width: 30px;
    height: 25px;
  }
  th {
    text-align: center;
  }
  .even {
    background: #fbf8f0;
  }
  .odd {
    background: #fefcf9;
  }
</style>

<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="caption">
        </div>
        <div class="actions">
          <!--div class="btn-group btn-group-devided" data-toggle="buttons" onclick="showModal()">
            <label class="btn btn-transparent grey-salsa  btn-sm active">
            <input type="radio" name="options" class="toggle" id="option1">Tambah Pengeluaran</label>
          </div-->
        </div>
      </div>
      <div class="portlet-body form">
        <div class="table-scrollable" >
          <table >
          <thead>
            <tr>
              <th>Hari</th>
              <th>Tanggal</th>
              <th>Nama Pegawai</th>
              <th>Proyek</th>
              <th>COA</th>
              <th>SPPD</th>
              <th>Debit</th>
              <th>Kredit</th>
              <th>Keterangan</th>
              <th>Nota</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <?php 

            $debit = 0;
            $kredit = 0 ;

            foreach($dataKeuangan as $val) { 

                $debit = $debit + $val['t_debit'];
                $kredit = $kredit + $val['t_kredit'];
            ?>
                <tr>
                  <td align="center">
                    <?=Yii::app()->myClass->FormatHariIndonesia($val['t_tanggal'])?>
                  </td>
                  <td align="center">
                    <?=Yii::app()->myClass->FormatTanggalIndonesia($val['t_tanggal'])?>
                  </td>
                  <td>
                    <?=$val['p_nama_lengkap']?>
                  </td>
                  <td>
                    <?=$val['p_nama']?>
                  </td>
                  <td>
                    <?=$val['c_nama']?>
                  </td>
                  <td align="center" style="width: 5px;">
                    <?php if($val['t_sppd'] == 0){ echo ""; } else{ echo "<i class='fa fa-check'></i>"; }?>
                  </td>
                  <td align="right">
                    <?php if($val['t_debit'] == 0){ echo ""; } else{ echo Yii::app()->myClass->FormatRupiah($val['t_debit']); }?>
                  </td>
                  <td align="right">
                  <?php if($val['t_kredit'] == 0){ echo ""; } else{ echo Yii::app()->myClass->FormatRupiah($val['t_kredit']); }?>
                  </td>
                  <td>
                    <?=$val['t_keterangan']?>
                  </td>
                  <td align="center">
                    <?php if($val['t_bukti'] != ""){ ?>
                    <a href='../bukti/<?=$val['t_bukti']?>' download><button class='btn btn-sm green'><i class='fa fa-arrow-down'></i></button></a>
                    <?php } ?>
                  </td>
                  <td style="width: 100px;" align="center">
                    <?php if($val['t_kredit'] != 0){ ?>
                      <a class="btn btn-sm red" onclick="deleteData('<?php echo $val["t_id"]; ?>')" title="Hapus">
                        <i class="fa fa-trash"></i> 
                      </a> 
                      <a class="btn btn-sm green" onclick="updateData('<?php echo $val["t_id"]; ?>')" title="Hapus">
                        <i class="fa fa-edit"></i> 
                      </a>
                    <?php }else{ ?>
                      <?php if(Yii::app()->user->getState('_level') == 'admin'){ ?>
                      <a class="btn btn-sm red" onclick="deleteData('<?php echo $val["t_id"]; ?>')" title="Hapus">
                        <i class="fa fa-trash"></i> 
                      </a>
                      <a class="btn btn-sm green" onclick="updateDataCashFlow('<?php echo $val["t_id"]; ?>')" title="Hapus">
                        <i class="fa fa-edit"></i> 
                      </a> 
                      <?php } ?>
                    <?php } ?>

                    <?php if($val['t_kredit'] == 0 && $val['t_debit'] == 0){ ?>
                      <a class="btn btn-sm red" onclick="deleteData('<?php echo $val["t_id"]; ?>')" title="Hapus">
                        <i class="fa fa-trash"></i> 
                      </a> 
                    <?php } ?>
                  </td>
                </tr>
              <?php } ?>
              <tr>
                <td colspan="6" align="right"> <b>Total</b> </td>
                <td align="right" class="bold"><?=Yii::app()->myClass->FormatRupiah($debit)?></td>
                <td align="right" class="bold"><?=Yii::app()->myClass->FormatRupiah($kredit)?></td>
                
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td colspan="6" align="right"> <b>Saldo</b> </td>
                <td align="right" class="bold" colspan="2"><?=Yii::app()->myClass->FormatRupiah($debit - $kredit)?></td>
                <td></td>
                <td></td>
              </tr>
          </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="stack2" class="modal fade modal-scroll" tabindex="-1" >
  <div class="modal-dialog" style="width: 800px;" >
    <div class="modal-content" >
      <?php $form=$this->beginWidget('CActiveForm', array(
          'id'=>'transaksi-form',
          'enableAjaxValidation'=>true, 
          'stateful'=>true, 
          'htmlOptions'=>array('enctype' => 'multipart/form-data')
        )); ?>
        <div class="modal-header" >
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">Tambah Pengeluaran</h4> 
        </div>
        
        <div class="modal-body" >
          
          <div class="form-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-body">
                  <div class="form-group">
                    <label>Tanggal</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                      </span>
                      <input name="Transaksi[t_tanggal]" class="form-control date-picker" placeholder="Tanggal Pengeluaran" type="text" data-date-format="dd-mm-yyyy" readonly>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-body">
                  <div class="form-group">
                    <label>Proyek</label>
                    <div class="input-group">
                      <?php 
                      
                      /*$criteria = new CDbCriteria();
                      $criteria->addCondition("p_status=1");
                      echo $form->dropDownList($modelTransaksi,'t_proyek',CHtml::listData(Proyek::model()->findAll($criteria),'p_id','p_nama'),array('class'=>'form-control  input-medium'));*/ ?>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-body">
                  <div class="form-group">
                    <label>Sppd</label>
                    <div class="input-group">
                      <div class="checkbox-list">
                        <label class="checkbox-inline">
                        <input type="checkbox" value="sppd" name="Transaksi[t_sppd]"></label>
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-body">
                  <div class="form-group">
                    <label>COA</label>
                    <div class="input-group">
                      <?php /*echo $form->dropDownList($modelTransaksi,'t_coa',CHtml::listData(Coa::model()->findAll(),'c_id','c_nama'),array('class'=>'form-control  input-small'));*/ ?>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-body">
                  <div class="form-group">
                    <label>Nominal</label>
                    <div class="input-group">
                      <input type="text" class="form-control  input-small" name="Transaksi[t_nominal]">
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-body">
                  <div class="form-group">
                    <label>Bukti / Nota</label>
                    <div class="input-group">
                      <div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn default btn-file">
                        <span class="fileinput-new">
                        Nota </span>
                        <span class="fileinput-exists">
                        Change </span>
                        <input type="file" name="t_bukti">
                        </span>
                        <span class="fileinput-filename">
                        </span>
                        &nbsp; <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput">
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-body">
                  <div class="form-group">
                    <label>Keterangan</label>
                    <textarea class="form-control" rows="3" name="Transaksi[t_keterangan]" placeholder="Keterangan"></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <?php echo CHtml::submitButton('Simpan',array('class'=> 'btn blue')); ?>
        </div>
      <?php $this->endWidget(); ?>
    </div>
  </div>
</div>

<div id="stack3" class="modal fade modal-scroll" tabindex="-1" >
  <div class="modal-dialog" style="width: 800px;" >
    <div class="modal-content" >
      <?php $form=$this->beginWidget('CActiveForm', array(
          'id'=>'transaksi-form',
          'enableAjaxValidation'=>true, 
          'stateful'=>true, 
          'htmlOptions'=>array('enctype' => 'multipart/form-data')
        )); ?>
        <div class="modal-header" >
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">Ubah Pengeluaran</h4> 
        </div>
        
        <div class="modal-body" >
          
          <div class="form-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-body">
                  <div class="form-group">
                    <label>Tanggal</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                      </span>
                      <input name="Transaksi[t_tanggal]" id="t_tanggal" class="form-control date-picker" placeholder="Tanggal Pengeluaran" type="text" data-date-format="dd-mm-yyyy" readonly>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-body">
                  <div class="form-group">
                    <label>Proyek</label>
                    <div class="input-group">
                      <?php 
                      
                      $criteria = new CDbCriteria();
                      $criteria->addCondition("p_status=1");
                      echo $form->dropDownList($modelTransaksi,'t_proyek',CHtml::listData(Proyek::model()->findAll($criteria),'p_id','p_nama'),array('class'=>'form-control  input-medium')); ?>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-body">
                  <div class="form-group">
                    <label>Sppd</label>
                    <div class="input-group">
                        <input type="checkbox" value="sppd" name="Transaksi[t_sppd]" id="t_sppd">
                        
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-body">
                  <div class="form-group">
                    <label>COA</label>
                    <div class="input-group">
                      <?php echo $form->dropDownList($modelTransaksi,'t_coa',CHtml::listData(Coa::model()->findAll(),'c_id','c_nama'),array('class'=>'form-control  input-small')); ?>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-body">
                  <div class="form-group">
                    <label>Nominal</label>
                    <div class="input-group">
                      <input type="text" class="form-control  input-small" name="Transaksi[t_nominal]" id="t_nominal">
                      <input type="hidden" class="form-control  input-small" name="Transaksi[t_id]" id="t_id">
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-body">
                  <div class="form-group">
                    <label>Bukti / Nota</label>
                    <div class="input-group">
                      <div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn default btn-file">
                        <span class="fileinput-new">
                        Nota </span>
                        <span class="fileinput-exists">
                        Change </span>
                        <input type="file" name="t_bukti">
                        </span>
                        <span class="fileinput-filename">
                        </span>
                        &nbsp; <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput">
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-body">
                  <div class="form-group">
                    <label>Keterangan</label>
                    <textarea class="form-control" rows="3" name="Transaksi[t_keterangan]" placeholder="Keterangan" id="t_keterangan"></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <?php echo CHtml::submitButton('Simpan',array('class'=> 'btn blue')); ?>
        </div>
      <?php $this->endWidget(); ?>
    </div>
  </div>
</div>

<div id="stack4" class="modal fade modal-scroll" tabindex="-1" >
  <div class="modal-dialog" style="width: 800px;" >
    <div class="modal-content" >
      <?php $form=$this->beginWidget('CActiveForm', array(
          'id'=>'transaksi-form',
          'action' => array( '/Keuangan/SaveCashFlow' ),
          'enableAjaxValidation'=>true, 
          'stateful'=>true, 
          'htmlOptions'=>array('enctype' => 'multipart/form-data')
        )); ?>
        <div class="modal-header" >
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">Ubah Cash Flow</h4> 
        </div>
        
        <div class="modal-body" >
          
          <div class="form-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-body">
                  <div class="form-group">
                    <label>Tanggal</label>
                    <div class="input-group">
                      <span class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                      </span>
                      <input name="Transaksi[t_tanggal_cash_flow]" id="t_tanggal_cash_flow" class="form-control date-picker" placeholder="Tanggal" type="text" data-date-format="dd-mm-yyyy" readonly>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-body">
                  <div class="form-group">
                    <label>Pegawai</label>
                    <div class="input-group">
                      <?php 
                      $criteria = new CDbCriteria();
                      $criteria->addCondition("p_level='pegawai'");
                      $criteria->addCondition("p_status=1");
                      echo $form->dropDownList($modelTransaksi,'t_pegawai',CHtml::listData(Pegawai::model()->findAll($criteria),'p_id','p_nama_lengkap'),array('class'=>'form-control  input-large')); ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-body">
                  <div class="form-group">
                    <label>Nominal</label>
                    <div class="input-group">
                      <input type="text" class="form-control  input-small" name="Transaksi[t_nominal_cash_flow]" id="t_nominal_cash_flow">
                      <input type="hidden" class="form-control  input-small" name="Transaksi[t_id_cash_flow]" id="t_id_cash_flow">
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-body">
                  <div class="form-group">
                    <label>Keterangan</label>
                    <textarea class="form-control" rows="3" name="Transaksi[t_keterangan_cash_flow]" placeholder="Keterangan" id="t_keterangan_cash_flow"></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <?php echo CHtml::submitButton('Simpan',array('class'=> 'btn blue')); ?>
        </div>
      <?php $this->endWidget(); ?>
    </div>
  </div>
</div>

<script  type="text/javascript">
  function showModal(){
      $("#stack2").modal('show');

      $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true
            });
  }

  function updateDataCashFlow(id){
    $.ajax({
          url: "<?php echo $this->createUrl('Keuangan/GetDataKeuangan') ?>",
          type: 'POST',
          data: {
              "SendData":{
                  "id": id, 
              }
          },
          success: function(data) { 
              var objJSON = eval("(function(){return " + data + ";})()");
              
              for (var i = objJSON.length - 1; i >= 0; i--) {


                var namaLengkap = objJSON[i].p_nama_lengkap;
                $("#t_id_cash_flow").val(id);
                $("#t_keterangan_cash_flow").val(objJSON[i].t_keterangan);
                $("#t_nominal_cash_flow").val(objJSON[i].t_debit);
                $("#t_tanggal_cash_flow").val(objJSON[i].t_tanggal);
                $("#Transaksi_t_pegawai").val(objJSON[i].t_pegawai);

              }
              $("#stack4").modal('show');

              $('.date-picker').datepicker({
                        rtl: Metronic.isRTL(),
                        orientation: "left",
                        autoclose: true
                    });
          },
          error: function(data) {
              alert("Error!");
              Metronic.unblockUI();
          }   
      });
  }

  function updateData(id){

      $.ajax({
          url: "<?php echo $this->createUrl('Keuangan/GetDataKeuangan') ?>",
          type: 'POST',
          data: {
              "SendData":{
                  "id": id, 
              }
          },
          success: function(data) { 
              var objJSON = eval("(function(){return " + data + ";})()");
              
              for (var i = objJSON.length - 1; i >= 0; i--) {


                var namaLengkap = objJSON[i].p_nama_lengkap;
                $("#t_id").val(id);
                $("#t_keterangan").val(objJSON[i].t_keterangan);
                $("#t_nominal").val(objJSON[i].t_kredit);
                $("#t_tanggal").val(objJSON[i].t_tanggal);
                $("#Transaksi_t_proyek").val(objJSON[i].t_proyek);
                $("#Transaksi_t_coa").val(objJSON[i].t_coa);

                if(objJSON[i].t_sppd == "1"){
                  $('#t_sppd').prop('checked', true);
                }else{
                  $('#t_sppd').prop('checked', false);
                } 
                
              //$("#t_keterangan").val(objJSON[i].t_keterangan);
              }
              $("#stack3").modal('show');

              $('.date-picker').datepicker({
                        rtl: Metronic.isRTL(),
                        orientation: "left",
                        autoclose: true
                    });
          },
          error: function(data) {
              alert("Error!");
              Metronic.unblockUI();
          }   
      });
  }

  function deleteData(id){


    bootbox.confirm("Are you sure?", function(result) {
       
       if(result == true){
        Metronic.blockUI({
            boxed: true
        });

        $.ajax({
            url: "<?php echo $this->createUrl('Keuangan/DeleteKeuangan') ?>",
            type: 'POST',
            data: {
                "SendData":{
                    "id": id, 
                }
            },
            success: function(data) { 
                
                Metronic.unblockUI();
                location.reload();
            },
            error: function(data) {
                alert("Error!");
                Metronic.unblockUI();
            }   
        });
       }
    }); 
  }
</script>