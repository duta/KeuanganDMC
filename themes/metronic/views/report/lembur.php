<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
Laporan Lembur
</h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-file-o"></i>
			<a href="">Laporan</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Lembur</a>
		</li>
	</ul>
</div>
<!-- END PAGE HEADER-->
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					Laporan Lembur
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="dataLemburSudahKonfirmasi">
					<thead>
					<tr>
					<th>
	                   Status
	                </th>
	                <th>
						 
					</th>
	                <th class="hidden-xs">
	                   Pegawai
	                </th>
	                <th class="hidden-xs">
	                  Jenis Lembur
	                </th>
	                <th class="hidden-xs">
	                   Hari, Tanggal
	                </th>
	                <th class="hidden-xs">
	                   Jam Mulai
	                </th>
	                <th class="hidden-xs">
	                   Total Lembur
	                </th>
	                <th class="hidden-xs" style="display:none;">
	                   Keterangan
	                </th>
	                <th class="hidden-xs" style="display:none;">
	                   Proyek
	                </th>
	                <th class="hidden-xs" style="display:none;">
	                   Admin Konfirmasi
	                </th>
					</tr>
					</thead>
					<tbody>
					<?php foreach($dataResult as $val) {   ?>
						<tr>
		                  <td align="center">
		                    <?php if($val['l_status'] == 0){?>
		                      <span class="label label-sm label-danger " title="">
		                      Belum Dibayar
		                      </span> 
		                    <?php }elseif ($val['l_status'] == 1) { ?>
		                      <span class="label label-sm label-success ">
		                      Sudah Dibayar
		                      </span>
		                    <?php }?>
		                  </td>
		                  <td align="center">
							<?php if($val['l_status'] == 0){?>
							<a class="btn btn-sm red" onclick="deleteData('<?php echo $val["l_id"]; ?>')" title="Hapus">
		                        <i class="fa fa-trash"></i> 
		                    </a>
							<?php }?>
						  </td>
		                  <td>
		                    <?=$val['pegawai']?>
		                  </td>
		                  <td>
		                    <?php 
		                      if($val['l_jenis'] == 1){
		                          echo "Hari Biasa";
		                      }else{
		                        echo "Hari Libur";
		                      }
		                    ?>
		                  </td>
		                  <td>
		                    <?=Yii::app()->myClass->FormatTanggalHariIndonesia($val['l_tanggal'])?>
		                  </td>
		                  <td>
		                    <?=date('H:i',strtotime($val['l_waktu']))?>
		                  </td>
		                  <td align="center">
		                    <?=$val['l_total_jam']?> jam
		                  </td>
		                  <td style="display:none;">
		                    <?=$val['l_keterangan']?>
		                  </td>
		                  <td style="display:none;">
		                    <?=$val['p_nama']?>
		                  </td>
		                  <td  style="display:none;">
		                     <?=$val['admin']?>
		                  </td>
		                </tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function deleteData(id){


    bootbox.confirm("Are you sure?", function(result) {
       
       if(result == true){
        Metronic.blockUI({
            boxed: true
        });

        $.ajax({
            url: "<?php echo $this->createUrl('Report/DeleteLembur') ?>",
            type: 'POST',
            data: {
                "SendData":{
                    "id": id, 
                }
            },
            success: function(data) { 
                
                Metronic.unblockUI();
                location.reload();
            },
            error: function(data) {
                alert("Error!");
                Metronic.unblockUI();
            }   
        });
       }
    }); 
  }
</script>