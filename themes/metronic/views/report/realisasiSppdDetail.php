<style type="text/css">
  table , td, th {
    border: 1px solid #E5E5E5;
    border-collapse: initial;
    width: 100%;
  }
  td, th {
    padding: 3px;
    width: 30px;
    height: 25px;
  }
  th {
    text-align: center;
  }
  .even {
    background: #fbf8f0;
  }
  .odd {
    background: #fefcf9;
  }
</style>

<h3 class="page-title">
Realisasi SPPD
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-users"></i>
      <a href="#">Realisasi SPPD</a>
    </li>
  </ul>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
      <div class="portlet-title">
        <div class="caption">
          <i class="icon-bar-chart font-green-sharp hide"></i>
          <span class="caption-subject font-green-sharp bold uppercase">Informasi</span>
          <span class="caption-helper">SPPD</span>
        </div>
        <div class="actions">
        </div>
      </div>
			<div class="portlet-body form">
				<!-- BEGIN FORM-->
				<form action="" method="POST" class="horizontal-form">
					<div class="form-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label">Keterangan</label>
									<label class="form-control">
										<?=$dataHeader['s_alasan']?>
									</label>
								</div>
							</div>
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Pegawai</label>
									<label class="form-control">
										<?=$dataHeader['p_nama_lengkap']?>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Proyek</label>
									<label class="form-control">
										<?=$dataHeader['proyek']?>
									</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Hari</label>
									<label class="form-control">
										<?=Yii::app()->myClass->FormatHariIndonesia($dataHeader['rsh_tanggal'])?>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Tanggal</label>
									<label class="form-control">
										<?=Yii::app()->myClass->FormatTanggalIndonesia($dataHeader['rsh_tanggal'])?>
									</label>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- END FORM-->
			</div>
		</div>
	</div>
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="caption">
          <i class="icon-bar-chart font-green-sharp hide"></i>
          <span class="caption-subject font-green-sharp bold uppercase">Realisasi</span>
          <span class="caption-helper">SPPD</span>
        </div>
        <div class="actions">
        </div>
      </div>
      <div class="portlet-body form">
        <div class="table-scrollable" >
          <table >
          <thead>
            <tr>
              <th>Hari</th>
              <th>Tanggal</th>
              <th>Nama Pegawai</th>
              <th>Proyek</th>
              <th>COA</th>
              <th>Debit</th>
              <th>Kredit</th>
              <th>Keterangan</th>
            </tr>
          </thead>
          <tbody>
            <?php 

            $debit = 0;
            $kredit = 0 ;

            foreach($dataKeuangan as $val) { 

                $debit = $debit + $val['t_debit'];
                $kredit = $kredit + $val['t_kredit'];
            ?>
                <tr>
                  <td align="center">
                    <?=Yii::app()->myClass->FormatHariIndonesia($val['t_tanggal'])?>
                  </td>
                  <td align="center">
                    <?=Yii::app()->myClass->FormatTanggalIndonesia($val['t_tanggal'])?>
                  </td>
                  <td>
                    <?=$val['p_nama_lengkap']?>
                  </td>
                  <td>
                    <?=$val['p_nama']?>
                  </td>
                  <td>
                    <?=$val['c_nama']?>
                  </td>
                  <td align="right">
                    <?php if($val['t_debit'] == 0){ echo ""; } else{ echo Yii::app()->myClass->FormatRupiah($val['t_debit']); }?>
                  </td>
                  <td align="right">
                  <?php if($val['t_kredit'] == 0){ echo ""; } else{ echo Yii::app()->myClass->FormatRupiah($val['t_kredit']); }?>
                  </td>
                  <td>
                    <?=$val['t_keterangan']?>
                  </td>
                </tr>
              <?php } ?>
              <tr>
                <td colspan="5" align="right"> <b>Total</b> </td>
                <td align="right" class="bold"><?=Yii::app()->myClass->FormatRupiah($debit)?></td>
                <td align="right" class="bold"><?=Yii::app()->myClass->FormatRupiah($kredit)?></td>
                <td align="right" class="bold"><?=Yii::app()->myClass->FormatRupiah($debit - $kredit)?></td>
                
              </tr>
          </tbody>
        </table>
        </div>

      </div>
    </div>
  </div>
</div>