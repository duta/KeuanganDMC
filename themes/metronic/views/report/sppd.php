<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
Laporan SPPD
</h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-file-o"></i>
			<a href="">Laporan</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">SPPD</a>
		</li>
	</ul>
</div>
<!-- END PAGE HEADER-->
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					Laporan SPPD
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="sample_3">
							<thead>
							<tr>
								<th>
									 Status
								</th>
								<th>
									 Tanggal Pengajuan
								</th>
								<th>
									 Nama Pegawai
								</th>
								<th>
									 Proyek
								</th>
								<th>
									 Alasan
								</th>
								<th>
									 Tanggal sppd
								</th>
								<th>
									 Admin Konfirmasi
								</th>
								<th>
									 
								</th>
							</tr>
							</thead>
							<tbody>
							<?php foreach($dataResult as $val) {   ?>
								<tr>
									<td align="center">
										<?php if($val['s_status_sppd'] == 0){?>
											<span class="label label-sm label-danger ">
											Belum Dibayar
											</span>
										<?php }elseif ($val['s_status_sppd'] == 1) { ?>
											<span class="label label-sm label-success ">
											Sudah Dibayar
											</span>
										<?php }?>
									</td>
									<td>
										<?=$val['s_tanggal_insert']?>
									</td>
									<td>
										<?=$val['pegawai']?>
									</td>
									<td>
										<?=$val['p_nama']?>
									</td>
									<td>
										<?=$val['s_alasan']?>
									</td>
									<td>
										<?php 
										$exp1=$val['s_tanggal'];										
										$expl1=explode(',',$val['s_tanggal']);
										for($i = 0; $i < count($expl1); $i++){
											//echo $expl1[$i];
											$detail = $expl1[$i];
											$expldDetail = explode('-', $detail);
											$perTanggal1 = date('d M Y',strtotime(str_replace('/','-',$expldDetail[0])));
											$perTanggal2 = date('d M Y',strtotime(str_replace('/','-',$expldDetail[1])));
											echo date('d M Y', strtotime(str_replace('/','-',$expldDetail[0])));
											echo " - ";
											echo date('d M Y', strtotime(str_replace('/','-',$expldDetail[1])));
											//echo "$expldDetail[1]";
											/*echo "$perTanggal1 - $perTanggal1";*/
											echo "<br/>";
										}										
										?>	
									</td>
									<td >
										 <?=$val['admin']?>
									</td>
									<td align="center">
										<?php if($val['s_status_sppd'] == 0){?>
											<a class="btn btn-sm red" onclick="deleteData('<?php echo $val["s_id"]; ?>')" title="Hapus">
						                        <i class="fa fa-trash"></i> 
						                      </a>
										<?php }?>
									</td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function deleteData(id){


    bootbox.confirm("Are you sure?", function(result) {
       
       if(result == true){
        Metronic.blockUI({
            boxed: true
        });

        $.ajax({
            url: "<?php echo $this->createUrl('Report/DeleteSppd') ?>",
            type: 'POST',
            data: {
                "SendData":{
                    "id": id, 
                }
            },
            success: function(data) { 
                
                Metronic.unblockUI();
                location.reload();
            },
            error: function(data) {
                alert("Error!");
                Metronic.unblockUI();
            }   
        });
       }
    }); 
  }
</script>