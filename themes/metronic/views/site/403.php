<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">       
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->
			<h3 class="page-title">
				Error 403
			</h3>
			<!--<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo $this->createUrl('site/index') ?>">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#">Error 403</a></li>
			</ul>-->
			<!-- END PAGE TITLE & BREADCRUMB-->
		</div>
	</div>
	<!-- END PAGE HEADER-->
	
	<div class="row-fluid">
		<div class="span12 page-500">
			<div class=" number">
				<font color="#183018">403</font>
			</div>
			<div class=" details">
				<h3>Anda Tidak Memiliki Hak Akses ke Halaman ini.</h3>
				<p>
					Untuk info lebih lanjut, hubungi Administrator.<br /><br />
				</p>
			</div>
		</div>
	</div>
</div>
