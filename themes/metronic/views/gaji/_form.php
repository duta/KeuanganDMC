<?php
/* @var $this GajiController */
/* @var $model Gaji */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gaji-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'g_pegawai'); ?>
		<?php echo $form->textField($model,'g_pegawai'); ?>
		<?php echo $form->error($model,'g_pegawai'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'g_tanggal'); ?>
		<?php echo $form->textField($model,'g_tanggal'); ?>
		<?php echo $form->error($model,'g_tanggal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'g_gaji'); ?>
		<?php echo $form->textField($model,'g_gaji'); ?>
		<?php echo $form->error($model,'g_gaji'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'g_gaji_perjam'); ?>
		<?php echo $form->textField($model,'g_gaji_perjam'); ?>
		<?php echo $form->error($model,'g_gaji_perjam'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'g_tanggal_insert'); ?>
		<?php echo $form->textField($model,'g_tanggal_insert'); ?>
		<?php echo $form->error($model,'g_tanggal_insert'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->