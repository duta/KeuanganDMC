<h3 class="page-title">
Ubah Data Jabatan
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="#">Master</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Data Jabatan</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Ubah Data</a>
    </li>
  </ul>
</div>

<?php $this->renderPartial('_form_shift', array('model'=>$model,'modelJKS'=>$modelJKS,'IDJabatan'=>$IDJabatan)); ?>