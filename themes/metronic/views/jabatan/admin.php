<h3 class="page-title">
Jabatan
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="#">Master</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Jabatan</a>
    </li>
  </ul>
</div>

<script type="text/javascript">
$(document).ready(function(){

	$('table.items').addClass('table-bordered table-striped table-condensed cf');
	$('div.summary').hide();
	$('th[id=jabatan-grid_c0]').css('width','5%');
	$('th[id=jabatan-grid_c1]').css('width','30%');

});
</script>

<div class="row ">
	<div class="col-md-12">
		<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="actions">
					<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/create'); ?>" class="btn btn-default btn-sm">
					<i class="fa fa-plus"></i> Tambah </a>
				</div>
			</div>
			<div class="portlet-body">
				<?php $this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'jabatan-grid',
					'dataProvider'=>$model->search(),
					'template' => "{items}{pager}",
					'enablePagination' => true,
					'columns'=>array(
						array('name' => 'number', 'header' => 'No.','value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1', 'htmlOptions' => array('style' => 'text-align:center')),
						array('name' => 'j_nama', 'header' => 'Nama Jabatan','value' => '$data[\'j_nama\']'),
						array(
						  'class'=>'zii.widgets.grid.CButtonColumn',
						  'cssClassExpression' => '"table-action-center"',
						  'template' => '{update} {delete}',
						  'viewButtonUrl'=>'Yii::app()->createUrl(Yii::app()->controller->id."/view", array("id"=>$data["j_id"]))',
						  'updateButtonUrl'=>'Yii::app()->createUrl(Yii::app()->controller->id."/update", array("id"=>$data["j_id"]))',
						  'deleteButtonUrl'=>'Yii::app()->createUrl(Yii::app()->controller->id."/delete", array("id"=>$data["j_id"]))',
						),
					),
				)); 

				?>
			</div>
		</div>
		<!-- END SAMPLE FORM PORTLET-->
	</div>
</div>
