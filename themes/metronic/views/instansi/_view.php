<?php
/* @var $this InstansiController */
/* @var $data Instansi */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('i_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->i_id), array('view', 'id'=>$data->i_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('i_nama')); ?>:</b>
	<?php echo CHtml::encode($data->i_nama); ?>
	<br />


</div>