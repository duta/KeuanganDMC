<?php
/* @var $this InstansiController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Instansis',
);

$this->menu=array(
	array('label'=>'Create Instansi', 'url'=>array('create')),
	array('label'=>'Manage Instansi', 'url'=>array('admin')),
);
?>

<h1>Instansis</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
