<?php
/* @var $this InstansiController */
/* @var $model Instansi */

$this->breadcrumbs=array(
	'Instansis'=>array('index'),
	$model->i_id,
);

$this->menu=array(
	array('label'=>'List Instansi', 'url'=>array('index')),
	array('label'=>'Create Instansi', 'url'=>array('create')),
	array('label'=>'Update Instansi', 'url'=>array('update', 'id'=>$model->i_id)),
	array('label'=>'Delete Instansi', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->i_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Instansi', 'url'=>array('admin')),
);
?>

<h1>View Instansi #<?php echo $model->i_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'i_id',
		'i_nama',
	),
)); ?>
