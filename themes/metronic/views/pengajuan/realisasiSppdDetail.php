<style type="text/css">
  table , td, th {
    border: 1px solid #E5E5E5;
    border-collapse: initial;
    width: 100%;
  }
  td, th {
    padding: 3px;
    width: 30px;
    height: 25px;
  }
  th {
    text-align: center;
  }
  .even {
    background: #fbf8f0;
  }
  .odd {
    background: #fefcf9;
  }
</style>

<h3 class="page-title">
Realisasi SPPD
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-users"></i>
      <a href="#">Realisasi SPPD</a>
    </li>
  </ul>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
      <div class="portlet-title">
        <div class="caption">
          <i class="icon-bar-chart font-green-sharp hide"></i>
          <span class="caption-subject font-green-sharp bold uppercase">Informasi</span>
          <span class="caption-helper">SPPD</span>
        </div>
        <div class="actions">
        </div>
      </div>
			<div class="portlet-body form">
				<!-- BEGIN FORM-->
				<form action="" method="POST" class="horizontal-form">
					<div class="form-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label">Keterangan</label>
									<label class="form-control">
										<?=$dataHeader['s_alasan']?>
									</label>
								</div>
							</div>
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Pegawai</label>
									<label class="form-control">
										<?=$dataHeader['p_nama_lengkap']?>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Proyek</label>
									<label class="form-control">
										<?=$dataHeader['proyek']?>
									</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Hari</label>
									<label class="form-control">
										<?=Yii::app()->myClass->FormatHariIndonesia($dataHeader['rsh_tanggal'])?>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Tanggal</label>
									<label class="form-control">
										<?=Yii::app()->myClass->FormatTanggalIndonesia($dataHeader['rsh_tanggal'])?>
									</label>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- END FORM-->
			</div>
		</div>
	</div>
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="caption">
          <i class="icon-bar-chart font-green-sharp hide"></i>
          <span class="caption-subject font-green-sharp bold uppercase">Realisasi</span>
          <span class="caption-helper">SPPD</span>
        </div>
        <div class="actions">
          <div class="btn-group btn-group-devided" data-toggle="buttons" onclick="showModal()">
            <label class="btn btn-transparent grey-salsa  btn-sm active">
            <input type="radio" name="options" class="toggle" id="option1">Tambah Pengeluaran</label>
          </div>
        </div>
      </div>
      <div class="portlet-body form">
        <div class="table-scrollable" >
          <table >
          <thead>
            <tr>
              <th>Hari</th>
              <th>Tanggal</th>
              <th>Nama Pegawai</th>
              <th>Proyek</th>
              <th>COA</th>
              <th>Debit</th>
              <th>Kredit</th>
              <th>Keterangan</th>
              <th>Hapus</th>
            </tr>
          </thead>
          <tbody>
            <?php 

            $debit = 0;
            $kredit = 0 ;

            foreach($dataKeuangan as $val) { 

                $debit = $debit + $val['t_debit'];
                $kredit = $kredit + $val['t_kredit'];
            ?>
                <tr>
                  <td align="center">
                    <?=Yii::app()->myClass->FormatHariIndonesia($val['t_tanggal'])?>
                  </td>
                  <td align="center">
                    <?=Yii::app()->myClass->FormatTanggalIndonesia($val['t_tanggal'])?>
                  </td>
                  <td>
                    <?=$val['p_nama_lengkap']?>
                  </td>
                  <td>
                    <?=$val['p_nama']?>
                  </td>
                  <td>
                    <?=$val['c_nama']?>
                  </td>
                  <td align="right">
                    <?php if($val['t_debit'] == 0){ echo ""; } else{ echo Yii::app()->myClass->FormatRupiah($val['t_debit']); }?>
                  </td>
                  <td align="right">
                  <?php if($val['t_kredit'] == 0){ echo ""; } else{ echo Yii::app()->myClass->FormatRupiah($val['t_kredit']); }?>
                  </td>
                  <td>
                    <?=$val['t_keterangan']?>
                  </td>
                  <td align="center">
                    <a class="btn btn-sm red" onclick="deleteData('<?php echo $val["t_id"]; ?>')">
                      <i class="fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
              <?php } ?>
              <tr>
                <td colspan="5" align="right"> <b>Total</b> </td>
                <td align="right" class="bold"><?=Yii::app()->myClass->FormatRupiah($debit)?></td>
                <td align="right" class="bold"><?=Yii::app()->myClass->FormatRupiah($kredit)?></td>
                <td align="right" class="bold"><?=Yii::app()->myClass->FormatRupiah($debit - $kredit)?></td>
                <td></td>
              </tr>
          </tbody>
        </table>
        </div>

      </div>
    </div>
  </div>
</div>
<script language="javascript">
function validasiData(){
      
  answer = confirm ("Are you sure you want to submit?")
  if (answer ==true) 
  { 
    return true;
  }else{
    return false;
  } 
  
 }
</script>
<div id="stack2" class="modal fade modal-scroll" tabindex="-1" >
  <div class="modal-dialog" style="width: 800px;" >
    <div class="modal-content" >
      <form role="form" method="POST" action="" onsubmit="return validasiData();" >
        <div class="modal-header" >
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">Tambah Pengeluaran</h4> 
        </div>
        <?php $form=$this->beginWidget('CActiveForm', array(
          'id'=>'transaksi-form',
          // Please note: When you enable ajax validation, make sure the corresponding
          // controller action is handling ajax validation correctly.
          // There is a call to performAjaxValidation() commented in generated controller code.
          // See class documentation of CActiveForm for details on this.
          'enableAjaxValidation'=>false,
        )); ?>
        <div class="modal-body" >
          
          <div class="form-body">

            <div class="row">
              <div class="col-md-6">
                <div class="form-body">
                  <div class="form-group">
                    <label>COA</label>
                    <div class="input-group">
                      <?php echo $form->dropDownList($modelTransaksi,'t_coa',CHtml::listData(Coa::model()->findAll(),'c_id','c_nama'),array('class'=>'form-control  input-large')); ?>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-body">
                  <div class="form-group">
                    <label>Nominal</label>
                    <div class="input-group">
                      <input type="text" class="form-control  input-large" name="Transaksi[t_nominal]">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-body">
                  <div class="form-group">
                    <label>Keterangan</label>
                    <textarea class="form-control" rows="3" name="Transaksi[t_keterangan]" placeholder="Keterangan"></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" data-dismiss="modal" class="btn">Batal</button>
          <?php echo CHtml::submitButton('Simpan',array('class'=> 'btn blue')); ?>
        </div>
      <?php $this->endWidget(); ?>
    </div>
  </div>
</div>



<script src="<?=Yii::app()->theme->baseUrl;?>/assets/admin/pages/scripts/components-form-tools.js"></script>

<script  type="text/javascript">
  function showModal(){
      $("#stack2").modal('show');
  }

  function deleteData(id){
    bootbox.confirm("Are you sure?", function(result) {
       
       if(result == true){
        Metronic.blockUI({
            boxed: true
        });

        $.ajax({
            url: "<?php echo $this->createUrl('Keuangan/DeleteKeuangan') ?>",
            type: 'POST',
            data: {
                "SendData":{
                    "id": id, 
                }
            },
            success: function(data) { 
                
                Metronic.unblockUI();
                location.reload();
            },
            error: function(data) {
                alert("Error!");
                Metronic.unblockUI();
            }   
        });
       }
    }); 
  }
</script>