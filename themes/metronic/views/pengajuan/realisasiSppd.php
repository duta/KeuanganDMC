<link rel="stylesheet" type="text/css" href="<?=Yii::app()->theme->baseUrl;?>/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<h3 class="page-title">
Realisasi SPPD
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="#">SPPD</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Realisasi</a>
    </li>
  </ul>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Realisasi SPPD
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
        </div>
      </div>
      <div class="portlet-body">
      	<table class="table table-striped table-bordered table-hover" id="sample_3">
              <thead>
              <tr>
                <th>
                   Pegawai
                </th>
                <th>
                   Hari, Tanggal
                </th>
                <th>
                   Proyek
                </th>
                <th>
                   Keterangan
                </th>
                <th>
                   Realisasi
                </th>
              </tr>
              </thead>
              <tbody>
              <?php foreach($dataSPPD as $val) {   ?>
                <tr>
                  <td>
                    <?=$val['p_nama_lengkap']?>
                  </td>
                  <td>
                    <?=Yii::app()->myClass->FormatTanggalHariIndonesia($val['rsh_tanggal'])?>
                  </td>
                  <td>
                    <?=$val['proyek']?>
                  </td>
                  <td>
                    <?=$val['s_alasan']?>
                  </td>
                  <td align="center">
                    <a class="btn btn-sm red" href="<?php echo $this->createUrl('Pengajuan/RealisasiDetail').'/'.$val['rsh_id'] ?>" >
                      <i class="fa fa-list-alt"></i> Realisasi
                    </a>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
      </div>
    </div>
  </div>
</div>

<div id="stack2" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-dialog" >
		<div class="modal-content" >
			<form action="<?php echo Yii::app()->baseUrl.'/Pengajuan/RealisasiSPPD' ?>" enctype="multipart/form-data" method="POST" class="form-horizontal form-bordered">
				<div class="modal-header" >
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title" id="judul"></h4> 
				</div>
				<div class="modal-body" >
					<input type="hidden" name="id_rsh" id="id_rsh">
					<div class="form-group">
						<label class="control-label col-md-3">Biaya Transportasi</label>
						<div class="col-md-6" style="padding-right: 0px;">
							<input type="number" class="form-control text-right" name="rss_biaya[]" />
							<input type="hidden" name="rss_jenis[]" value="transport" />
						</div>
						<div class="col-md-3" style="padding-left: 0px;">
						<!--input type="file" name="file_transport"-->
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<span class="btn default btn-file">
								<span class="fileinput-new">
								Nota </span>
								<span class="fileinput-exists">
								Change </span>
								<input type="file" name="file_transport">
								</span>
								<span class="fileinput-filename">
								</span>
								&nbsp; <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput">
								</a>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Biaya Penginapan</label>
						<div class="col-md-6" style="padding-right: 0px;">
							<input type="number" class="form-control text-right" name="rss_biaya[]" />
							<input type="hidden" name="rss_jenis[]" value="penginapan" />
						</div>
						<div class="col-md-3" style="padding-left: 0px;">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<span class="btn default btn-file">
								<span class="fileinput-new">
								Nota </span>
								<span class="fileinput-exists">
								Change </span>
								<input type="file" name="file_penginapan">
								</span>
								<span class="fileinput-filename">
								</span>
								&nbsp; <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput">
								</a>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Biaya Dari dan Ke</label>
						<div class="col-md-9">
							<input type="number" class="form-control text-right" name="rss_biaya[]" value=400000 />
							<input type="hidden" name="rss_jenis[]" value="dari dan ke" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Uang Makan</label>
						<input type="hidden" name="rss_jenis[]" value="uang makan" />
						<div class="col-md-9">
							<div class="checkbox-list">
								<label class="checkbox-inline">
								<input type="checkbox" name="cb[]" value="1">Pagi</label>
								<label class="checkbox-inline">
								<input type="checkbox" name="cb[]" value="1">Siang</label>
								<label class="checkbox-inline">
								<input type="checkbox" name="cb[]" value="1">Malam</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Uang Saku</label>
						<div class="col-md-9">
							<input type="number" class="form-control text-right" value=75000 disabled/>
							<input type="hidden" name="rss_jenis[]" value="uang saku" />
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" data-dismiss="modal" class="btn">Batal</button>
					<button type="button submit" class="btn red">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>


<script src="<?=Yii::app()->theme->baseUrl;?>/assets/admin/pages/scripts/components-form-tools.js"></script>

<script  type="text/javascript">
	function showModal(parm1,parm2){
	    $("#id_rsh").val(parm1);
	    $("#judul").html("Realisasi SPPD " + parm2);
	    $("#stack2").modal('show');
	}
</script>