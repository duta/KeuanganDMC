<?php
/* @var $this KeuanganController */
/* @var $data Transaksi */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('t_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->t_id), array('view', 'id'=>$data->t_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('t_tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->t_tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('t_pegawai')); ?>:</b>
	<?php echo CHtml::encode($data->t_pegawai); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('t_coa')); ?>:</b>
	<?php echo CHtml::encode($data->t_coa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('t_proyek')); ?>:</b>
	<?php echo CHtml::encode($data->t_proyek); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('t_debit')); ?>:</b>
	<?php echo CHtml::encode($data->t_debit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('t_kredit')); ?>:</b>
	<?php echo CHtml::encode($data->t_kredit); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('t_keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->t_keterangan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('t_tanggal_insert')); ?>:</b>
	<?php echo CHtml::encode($data->t_tanggal_insert); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('t_tanggal_update')); ?>:</b>
	<?php echo CHtml::encode($data->t_tanggal_update); ?>
	<br />

	*/ ?>

</div>