<?php
/* @var $this KeuanganController */
/* @var $model Transaksi */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'transaksi-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'t_tanggal'); ?>
		<?php echo $form->textField($model,'t_tanggal'); ?>
		<?php echo $form->error($model,'t_tanggal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'t_pegawai'); ?>
		<?php echo $form->textField($model,'t_pegawai'); ?>
		<?php echo $form->error($model,'t_pegawai'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'t_coa'); ?>
		<?php echo $form->textField($model,'t_coa'); ?>
		<?php echo $form->error($model,'t_coa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'t_proyek'); ?>
		<?php echo $form->textField($model,'t_proyek'); ?>
		<?php echo $form->error($model,'t_proyek'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'t_debit'); ?>
		<?php echo $form->textField($model,'t_debit'); ?>
		<?php echo $form->error($model,'t_debit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'t_kredit'); ?>
		<?php echo $form->textField($model,'t_kredit'); ?>
		<?php echo $form->error($model,'t_kredit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'t_keterangan'); ?>
		<?php echo $form->textField($model,'t_keterangan',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'t_keterangan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'t_tanggal_insert'); ?>
		<?php echo $form->textField($model,'t_tanggal_insert'); ?>
		<?php echo $form->error($model,'t_tanggal_insert'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'t_tanggal_update'); ?>
		<?php echo $form->textField($model,'t_tanggal_update'); ?>
		<?php echo $form->error($model,'t_tanggal_update'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->