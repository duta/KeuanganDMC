<link rel="stylesheet" type="text/css" href="<?=Yii::app()->theme->baseUrl;?>/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<h3 class="page-title">
Realisasi SPPD
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="#">SPPD</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Realisasi</a>
    </li>
  </ul>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Realisasi SPPD
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
        </div>
      </div>
      <div class="portlet-body">
      	<table class="table table-striped table-bordered table-hover" id="sample_3">
              <thead>
              <tr>
                <th>
                   Realisasi
                </th>
                <th>
                   Tanggal SPPD
                </th>
                <th>
                   Pegawai
                </th>
                <th>
                   Keterangan
                </th>
                <th>
                   Keterangan
                </th>
                <th>
                   Total
                </th>
              </tr>
              </thead>
              <tbody>
              <?php foreach($dataSPPD as $val) {   ?>
                <tr>
                  <td align="center">
                    <a class="btn btn-sm red" onclick="showModal('<?php echo $val["rsh_id"]; ?>','<?php echo Yii::app()->myClass->FormatTanggalHariIndonesia($val["rsh_tanggal"]); ?>')">
                      <i class="fa fa-list-alt"></i> Realisasi
                    </a>
                  </td>
                  <td>
                    <?=Yii::app()->myClass->FormatTanggalIndonesia($val['rsh_tanggal'])?>
                  </td>
                  <td>
                    <?=$val['p_nama_lengkap']?>
                  </td>
                  <td>
                    <?=$val['proyek']?>
                  </td>
                  <td>
                    <?=$val['s_alasan']?>
                  </td>
                  <td>
                    <?=Yii::app()->myClass->FormatRupiah($val['rsh_total'])?>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
      </div>
    </div>
  </div>
</div>

<div id="stack2" class="modal fade" tabindex="-1" data-width="400">
	<div class="modal-dialog" >
		<div class="modal-content form-horizontal form-bordered" >
			
				<div class="modal-header" >
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title" id="judul"></h4> 
				</div>
				<div class="modal-body" >
					<div class="form-group">
						<label class="control-label col-md-3">Biaya Transportasi</label>
						<div class="col-md-7">
							<input type="number" class="form-control text-right" id="transport" value=0 />
						</div>
						<div class="col-md-2" id="buktiTransport">
							
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Biaya Penginapan</label>
						<div class="col-md-7">
							<input type="number" class="form-control text-right" id="penginapan" value=0 />
						</div>
						<div class="col-md-2" id="buktiPenginapan">
							
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Biaya Dari dan Ke</label>
						<div class="col-md-9">
							<input type="number" class="form-control text-right" id="darike" value=0 />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Uang Makan</label>
						<div class="col-md-9">
							<input type="number" class="form-control text-right" id="makan" value=0  />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Uang Saku</label>
						<div class="col-md-9">
							<input type="number" class="form-control text-right" id="saku" value=0  />
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" data-dismiss="modal" class="btn">Batal</button>
					<button type="button submit" class="btn red" onclick="konfirmasiRealisasi()">Simpan</button>
				</div>
		</div>
	</div>
</div>

<input type="hidden" name="id_rsh" id="id_rsh">


<script src="<?=Yii::app()->theme->baseUrl;?>/assets/admin/pages/scripts/components-form-tools.js"></script>

<script  type="text/javascript">
	function showModal(id,tanggal){
	    $("#id_rsh").val(id);
	    $("#judul").html("Realisasi SPPD " + tanggal);
	    Metronic.blockUI({
	        boxed: true
	    });

		$.ajax({
			url: "<?php echo $this->createUrl('Konfirmasi/DetailRealisasi'); ?>",
			type: 'POST', 
			data : {
				'rs_header' : $("#id_rsh").val()
			},
			success: function(data) {
				var objJSON = eval("(function(){return " + data + ";})()");
				$("#transport").val(0);
				$("#buktiTransport").html("");
				$("#penginapan").val(0);
				$("#buktiPenginapan").html("");
				$("#darike").val(0);
				$("#makan").val(0);
				$("#saku").val(0);
				for (var i = objJSON.length - 1; i >= 0; i--) {
					if (objJSON[i].rs_jenis == "dari dan ke"){
						$("#darike").val(objJSON[i].rs_biaya);
					}
					if (objJSON[i].rs_jenis == "uang makan"){
						$("#makan").val(objJSON[i].rs_biaya);
					}
					if (objJSON[i].rs_jenis == "uang saku"){
						$("#saku").val(objJSON[i].rs_biaya);
					}
					if (objJSON[i].rs_jenis == "transport"){
						$("#transport").val(objJSON[i].rs_biaya);
						if(objJSON[i].rs_file != ""){
							$("#buktiTransport").html("<a href='../bukti/"+objJSON[i].rs_file+"' download><button class='btn green'><i class='fa fa-arrow-down'></i></button></a>");
							$("#buktiTransport").append("<input id='file_transport' type='hidden' value='"+objJSON[i].rs_file+"' />");
						}
					}
					if (objJSON[i].rs_jenis == "penginapan"){
						$("#penginapan").val(objJSON[i].rs_biaya);
						if(objJSON[i].rs_file != ""){
							$("#buktiPenginapan").html("<a href='../bukti/"+objJSON[i].rs_file+"' download><button class='btn green'><i class='fa fa-arrow-down'></i></button></a>");
							$("#buktiPenginapan").append("<input id='file_penginapan' type='hidden' value='"+objJSON[i].rs_file+"' />");
						}
					}
				}
				$("#stack2").modal('show');
				Metronic.unblockUI();
			},cache: false
		});
	}

	function konfirmasiRealisasi(){

		Metronic.blockUI({
            boxed: true
        });

		$.ajax({
			url: "<?php echo $this->createUrl('Konfirmasi/KonfirmasiRealisasi'); ?>",
			type: 'POST', 
			data : {
				'rs_header' : $("#id_rsh").val(),
				'transport' : $("#transport").val(),
				'file_transport' : $("#file_transport").val(),
				'penginapan' : $("#penginapan").val(),
				'file_penginapan' : $("#file_penginapan").val(),
				'darike' : $("#darike").val(),
				'makan' : $("#makan").val(),
				'saku' : $("#saku").val()
			},
			success: function(data) {
				location.reload();
			},cache: false  
		}); 
	}
</script>