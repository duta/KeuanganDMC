<?php 
	$uriMain = explode('/',$_SERVER["REQUEST_URI"]); 
	$level = Yii::app()->user->getState('_level');
	$statusUser = Yii::app()->user->getState('statusUser');
?>
<!-- BEGIN SIDEBAR MENU -->
<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
<ul class="page-sidebar-menu page-sidebar-menu-light" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
	<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
	<li class="sidebar-toggler-wrapper">
		<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
		<div class="sidebar-toggler">
		</div>
		<!-- END SIDEBAR TOGGLER BUTTON -->
	</li>
	<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
	<li class="sidebar-search-wrapper">
		<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
		<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
		<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
		<form class="sidebar-search " method="POST">
			<a href="javascript:;" class="remove">
			<i class="icon-close"></i>
			</a>
		</form>
		<!-- END RESPONSIVE QUICK SEARCH FORM -->
	</li>
	<li class="<?php if($uriMain[1]=='Dashboard'){ echo 'start active open'; } ?>">
		<a href="<?php echo $this->createUrl('Dashboard/Dashboard') ?>" >
		<i class="icon-home"></i>
		<span class="title">
		Dashboard </span>
		<?php if($uriMain[1]=='Dashboard'){ echo '<span class="selected"></span>'; } ?>
		</a>
	</li>
	<li >
		<a href="<?php echo $this->createUrl('Keuangan/Pengeluaran') ?>" >
		<i class="fa fa-money"></i>
		<span class="title">
		Pengeluaran </span>
		</a>
	</li>
	<?php if(strtoupper($level) == strtoupper('pegawai')){ ?>
	<li >
		<a href="<?php echo $this->createUrl('Pengajuan/Lembur') ?>" >
		<i class="fa fa-calendar"></i>
		<span class="title">
		Lembur </span>
		</a>
	</li>
	<li >
		<a href="<?php echo $this->createUrl('Pengajuan/Sppd') ?>" >
		<i class="fa fa-calendar"></i>
		<span class="title">
		SPPD </span>
		</a>
	</li>
	<!-- <li >
		<a href="<?php echo $this->createUrl('Pengajuan/Realisasi') ?>" >
		<i class="fa fa-list-alt"></i>
		<span class="title">
		Realisasi SPPD </span>
		</a>
	</li> -->
	<?php } ?>
	<?php if(strtoupper($level) == strtoupper('admin')){ ?>
	<li class="<?php if(strtoupper($uriMain[1])==strtoupper('Operasional')){ echo 'active open'; } ?>">
		<a href="javascript:;">
		<i class="icon-briefcase"></i>
		<span class="title"> Operasional</span>
		<?php if($uriMain[1]=='Operasional'){ echo '<span class="selected"></span>'; } ?>
		<span class="arrow "></span>
		</a>
		<ul class="sub-menu">
			<li>
				<a href="<?php echo $this->createUrl('Operasional/Kas') ?>">
				Cash Flow</a>
			</li>
			<li>
				<a href="<?php echo $this->createUrl('Operasional/Sppd') ?>">
				SPPD</a>
			</li>
			<li>
				<a href="<?php echo $this->createUrl('Operasional/Lembur') ?>">
				Lembur</a>
			</li>

		</ul>
	</li>
	<li class="<?php if(strtoupper($uriMain[2])==strtoupper('Admin')){ echo 'active open'; } ?>">
		<a href="javascript:;">
		<i class="icon-briefcase"></i>
		<span class="title"> Data Master</span>
		<?php if($uriMain[1]=='Admin'){ echo '<span class="selected"></span>'; } ?>
		<span class="arrow "></span>
		</a>
		<ul class="sub-menu">
			<li>
				<a href="<?php echo $this->createUrl('Pegawai/Admin') ?>">
				Pegawai</a>
			</li>
			<li>
				<a href="<?php echo $this->createUrl('Jabatan/Admin') ?>">
				Jabatan Pegawai</a>
			</li>
			<li>
				<a href="<?php echo $this->createUrl('Instansi/Admin') ?>">
				Instansi</a>
			</li>
			<li>
				<a href="<?php echo $this->createUrl('Proyek/Admin') ?>">
				Proyek</a>
			</li>
			<li>
				<a href="<?php echo $this->createUrl('Coa/Admin') ?>">
				COA</a>
			</li>
		</ul>
	</li>
	<?php } ?>

	<li class="last <?php if(strtoupper($uriMain[1])==strtoupper('Report')){ echo 'active open'; } ?>">
		<a href="javascript:;">
		<i class="fa fa-file-o"></i>
		<span class="title"> Laporan</span>
		<?php if(strtoupper($uriMain[1])==strtoupper('Report')){ echo '<span class="selected"></span>'; } ?>
		<span class="arrow "></span>
		</a>
		<ul class="sub-menu">
			<li>
				<a href="<?php echo $this->createUrl('Report/LemburPerBulan') ?>">
				Laporan Lembur</a>
			</li>
			<li>
				<a href="<?php echo $this->createUrl('Report/SppdPerBulan') ?>">
				Laporan SPPD</a>
			</li>
			<li>
				<a href="<?php echo $this->createUrl('Report/Keuangan') ?>">
				Laporan Keuangan</a>
			</li>
			<li>
				<a href="<?php echo $this->createUrl('Report/Jurnal') ?>">
				Laporan Jurnal</a>
			</li>
			<?php if(strtoupper($level) == strtoupper('admin')){ ?>
			<li>
				<a href="<?php echo $this->createUrl('Report/Rekap') ?>">
				Laporan Rekap</a>
			</li>
			<?php } ?>
			<?php if(strtoupper($level) == strtoupper('pegawai')){ ?>
			<li>
				<a href="<?php echo $this->createUrl('Report/Lembur') ?>">
				Lembur</a>
			</li>
			<li>
				<a href="<?php echo $this->createUrl('Report/Sppd') ?>">
				SPPD</a>
			</li>
			<?php } ?> 
			<!-- <li>
				<a href="<?php echo $this->createUrl('Report/RealisasiSppd') ?>">
				Realisasi SPPD</a>
			</li> -->
		</ul>
	</li>
</ul>
<!-- END SIDEBAR MENU -->
